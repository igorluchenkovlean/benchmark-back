import { Sequelize } from 'sequelize-typescript';
import { config } from 'dotenv';
import { resolve } from 'path';

import dbModels from './models';
import { makeSeeds } from './seeds';

config({ path: resolve(__dirname, '..', '..', '.env') });

export const models = dbModels;

const sequelize = new Sequelize({
	username: process.env.DB_USER,
	password: process.env.DB_PASSWORD,
	database: process.env.DB_DATABASE,
	host: process.env.DB_HOST,
	dialect: 'postgres',
	logging: false,
	define: {
		timestamps: false,
	},
	dialectOptions: {
		dateStrings: true,
		typeCast: true,
	},
	models: [__dirname + '/models/**/**.model.*'],
});

sequelize.sync({ force: true }).then(makeSeeds);

export default sequelize;
