import { Table, Column, Model } from 'sequelize-typescript';

@Table
export default class KPIField extends Model<KPIField> {
	@Column({ primaryKey: true, autoIncrement: true })
	id: number;

	@Column
	formula: string;
}
