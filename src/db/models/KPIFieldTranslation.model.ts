import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';

import Language from './language.model';
import KPIField from './KPIField.model';

@Table
export default class KPIFieldTranslation extends Model<KPIFieldTranslation> {
	@ForeignKey(() => Language)
	@Column
	languageCode: number;

	@BelongsTo(() => Language)
	language: Language;

	@ForeignKey(() => KPIField)
	@Column
	kpiFieldId: number;

	@BelongsTo(() => KPIField)
	kpiField: KPIField;

	@Column
	name: string;
}
