import { Table, Column, Model, BelongsTo, Default, DataType, ForeignKey } from 'sequelize-typescript';

import IndustryTranslation from 'db/models/industryTranslation.model';

@Table
export default class Company extends Model<Company> {
	@Column({ primaryKey: true, autoIncrement: true })
	id: number;

	@Column
	name: string;

	@Column
	employeesNumber: number;

	@Column
	country: string;

	@Column
	city: string;

	@Column
	postalCode: string;

	@ForeignKey(() => IndustryTranslation)
	@Column
	industryId: number;

	@BelongsTo(() => IndustryTranslation)
	industry: IndustryTranslation;

	@Default(DataType.NOW)
	@Column
	createdAt: Date;
}
