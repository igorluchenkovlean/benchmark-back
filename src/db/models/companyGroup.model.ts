import { Table, Column, Model, Default, DataType, ForeignKey, BelongsTo, HasMany } from 'sequelize-typescript';

import User from './user.model';
import GroupTranslation from './groupTranslation.model';
import CompanyGroupService from './companyGroupService.model';

@Table
export default class CompanyGroup extends Model<CompanyGroup> {
	@Column({ primaryKey: true, autoIncrement: true })
	id: number;

	@ForeignKey(() => User)
	@Column
	userId: number;

	@BelongsTo(() => User)
	user: User;

	@ForeignKey(() => GroupTranslation)
	@Column
	groupId: number;

	@BelongsTo(() => GroupTranslation)
	group: GroupTranslation;

	@HasMany(() => CompanyGroupService, 'companyGroupId')
	services: CompanyGroupService[];

	@Column
	updatedAt: Date;

	@Default(DataType.NOW)
	@Column
	createdAt: Date;
}
