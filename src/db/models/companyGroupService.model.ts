import { Table, Column, Model, Default, DataType, ForeignKey, BelongsTo } from 'sequelize-typescript';

import CompanyGroup from './companyGroup.model';
import ServiceTranslation from './serviceTranslation.model';

@Table
export default class CompanyGroupService extends Model<CompanyGroupService> {
	@Column({ primaryKey: true, autoIncrement: true })
	id: number;

	@ForeignKey(() => CompanyGroup)
	@Column
	companyGroupId: number;

	@BelongsTo(() => CompanyGroup)
	companyGroup: CompanyGroup;

	@ForeignKey(() => ServiceTranslation)
	@Column
	serviceId: number;

	@BelongsTo(() => ServiceTranslation)
	service: ServiceTranslation;

	@Column
	updatedAt: Date;

	@Default(DataType.NOW)
	@Column
	createdAt: Date;
}
