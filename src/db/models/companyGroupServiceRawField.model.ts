import { Table, Column, Model, DataType, Default, ForeignKey, BelongsTo } from 'sequelize-typescript';

import CompanyGroupService from './companyGroupService.model';
import GroupServiceRawField from './groupServiceRawField.model';

@Table
export default class CompanyGroupServiceRawField extends Model<CompanyGroupServiceRawField> {
	@Column({ primaryKey: true, autoIncrement: true })
	id: number;

	@ForeignKey(() => CompanyGroupService)
	@Column
	companyGroupServiceId: number;

	@BelongsTo(() => CompanyGroupService)
	companyGroupService: CompanyGroupService;

	@ForeignKey(() => GroupServiceRawField)
	@Column
	groupServiceRawFieldId: number;

	@BelongsTo(() => GroupServiceRawField)
	groupServiceRawField: GroupServiceRawField;

	@Column(DataType.FLOAT)
	value: number;

	@Column
	updatedAt: Date;

	@Default(DataType.NOW)
	@Column
	createdAt: Date;
}
