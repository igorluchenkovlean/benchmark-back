import { Table, Column, Model } from 'sequelize-typescript';

@Table
export default class DerivativeField extends Model<DerivativeField> {
	@Column({ primaryKey: true, autoIncrement: true })
	id: number;

	@Column
	formula: string;
}
