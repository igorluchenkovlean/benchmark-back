import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';

import Language from './language.model';
import DerivativeField from './derivativeField.model';

@Table
export default class DerivativeFieldTranslation extends Model<DerivativeFieldTranslation> {
	@ForeignKey(() => Language)
	@Column
	languageCode: number;

	@BelongsTo(() => Language)
	language: Language;

	@ForeignKey(() => DerivativeField)
	@Column
	derivativeFieldId: number;

	@BelongsTo(() => DerivativeField)
	derivativeField: DerivativeField;

	@Column
	name: string;
}
