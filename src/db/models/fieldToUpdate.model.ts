import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';

import PendingDataRequest from './pendingDataRequest.model';
import CompanyGroupServiceRawField from './companyGroupServiceRawField.model';

@Table
export default class FieldToUpdate extends Model<FieldToUpdate> {
	@ForeignKey(() => CompanyGroupServiceRawField)
	@Column
	companyGroupServiceRawFieldId: number;

	@BelongsTo(() => CompanyGroupServiceRawField)
	companyGroupServiceRawField: CompanyGroupServiceRawField;

	@ForeignKey(() => PendingDataRequest)
	@Column
	pendingDataRequestId: number;

	@BelongsTo(() => PendingDataRequest)
	pendingDataRequest: PendingDataRequest;

	@Column
	value: string;
}
