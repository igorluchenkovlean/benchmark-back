import { Table, Column, Model } from 'sequelize-typescript';

@Table
export default class Group extends Model<Group> {
	@Column({ primaryKey: true, autoIncrement: true })
	id: number;
}
