import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';

import Service from './serviceTranslation.model';
import Group from './groupTranslation.model';
import Numerator from './numeratorTranslation.model';

@Table
export default class GroupService extends Model<GroupService> {
	@Column({ primaryKey: true, autoIncrement: true })
	id: number;

	@ForeignKey(() => Group)
	@Column
	groupId: number;

	@BelongsTo(() => Group)
	group: Group;

	@ForeignKey(() => Service)
	@Column
	serviceId: number;

	@BelongsTo(() => Service)
	service: Service;

	@ForeignKey(() => Numerator)
	@Column
	numeratorId: number;

	@BelongsTo(() => Numerator)
	numerator: Numerator;
}
