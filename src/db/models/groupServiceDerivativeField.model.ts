import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';

import GroupService from './groupService.model';
import DerivativeField from './derivativeField.model';

@Table
export default class GroupServiceDerivativeField extends Model<GroupServiceDerivativeField> {
	@ForeignKey(() => GroupService)
	@Column
	serviceGroupId: number;

	@BelongsTo(() => GroupService)
	serviceGroup: GroupService;

	@ForeignKey(() => DerivativeField)
	@Column
	derivativeFieldId: number;

	@BelongsTo(() => DerivativeField)
	derivativeField: DerivativeField;
}
