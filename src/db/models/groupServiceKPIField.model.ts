import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';

import GroupService from './groupService.model';
import KPIField from './KPIField.model';

@Table
export default class GroupServiceKPIField extends Model<GroupServiceKPIField> {
	@ForeignKey(() => GroupService)
	@Column
	serviceGroupId: number;

	@BelongsTo(() => GroupService)
	serviceGroup: GroupService;

	@ForeignKey(() => KPIField)
	@Column
	kpiFieldId: number;

	@BelongsTo(() => KPIField)
	kpiField: KPIField;
}
