import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';

import GroupService from './groupService.model';
import RawField from './rawFieldTranslation.model';

@Table
export default class GroupServiceRawField extends Model<GroupServiceRawField> {
	@ForeignKey(() => GroupService)
	@Column
	serviceGroupId: number;

	@BelongsTo(() => GroupService)
	serviceGroup: GroupService;

	@ForeignKey(() => RawField)
	@Column
	rawFieldId: number;

	@BelongsTo(() => RawField)
	rawField: RawField;
}
