import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';

import Language from './language.model';
import Group from './group.model';

@Table
export default class GroupTranslation extends Model<GroupTranslation> {
	@ForeignKey(() => Language)
	@Column
	languageCode: number;

	@BelongsTo(() => Language)
	language: Language;

	@ForeignKey(() => Group)
	@Column
	groupId: number;

	@BelongsTo(() => Group)
	group: Group;

	@Column
	name: string;
}
