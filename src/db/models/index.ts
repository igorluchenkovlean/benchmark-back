import User from './user.model';
import Privelege from './privelege.model';
import Role from './role.model';
import RolePrivelege from './rolePrivelege.model';
import Language from './language.model';
import DerivativeField from './derivativeField.model';
import DerivativeFieldTranslation from './derivativeFieldTranslation.model';
import Group from './group.model';
import GroupService from './groupService.model';
import GroupServiceDerivativeField from './groupServiceDerivativeField.model';
import GroupServiceKPIField from './groupServiceKPIField.model';
import GroupServiceRawField from './groupServiceRawField.model';
import GroupTranslation from './groupTranslation.model';
import KPIField from './KPIField.model';
import KPIFieldTranslation from './KPIFieldTranslation.model';
import Numerator from './numerator.model';
import NumeratorTranslation from './numeratorTranslation.model';
import RawField from './rawField.model';
import RawFieldTranslation from './rawFieldTranslation.model';
import Service from './service.model';
import ServiceTranslation from './serviceTranslation.model';
import Company from './company.model';
import Industry from './industry.model';
import CompanyGroupService from './companyGroupService.model';
import CompanyGroupServiceRawField from './companyGroupServiceRawField.model';
import FieldToUpdate from './fieldToUpdate.model';
import IndustryTranslation from './industryTranslation.model';
import PendingDataRequest from './pendingDataRequest.model';
import RegistrationStatus from './registrationStatus.model';
import ReservedDomain from './reservedDomain.model';
import UserRegistrationStatus from './userRegistrationStatus.model';
import UserRole from './userRole.model';
import CompanyGroup from './companyGroup.model';

export default {
	User,
	Privelege,
	Role,
	RolePrivelege,
	Language,
	DerivativeField,
	DerivativeFieldTranslation,
	Group,
	GroupService,
	GroupServiceDerivativeField,
	GroupServiceKPIField,
	GroupServiceRawField,
	GroupTranslation,
	KPIField,
	KPIFieldTranslation,
	Numerator,
	NumeratorTranslation,
	RawField,
	RawFieldTranslation,
	Service,
	ServiceTranslation,
	Company,
	Industry,
	CompanyGroupService,
	CompanyGroupServiceRawField,
	FieldToUpdate,
	IndustryTranslation,
	PendingDataRequest,
	RegistrationStatus,
	ReservedDomain,
	UserRegistrationStatus,
	UserRole,
	CompanyGroup,
};
