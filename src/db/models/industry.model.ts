import { Table, Column, Model } from 'sequelize-typescript';

@Table
export default class Industry extends Model<Industry> {
	@Column({ primaryKey: true, autoIncrement: true })
	id: number;
}
