import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';

import Language from './language.model';
import Industry from './industry.model';

@Table
export default class IndustryTranslation extends Model<IndustryTranslation> {
	@ForeignKey(() => Industry)
	@Column
	industryId: number;

	@BelongsTo(() => Industry)
	industry: Industry;

	@ForeignKey(() => Language)
	@Column
	languageCode: number;

	@BelongsTo(() => Language)
	language: Language;

	@Column
	name: string;

	@Column
	description: string;
}
