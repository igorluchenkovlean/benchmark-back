import { Table, Column, Model } from 'sequelize-typescript';

import { UserLanguage } from 'typedefs/user';

@Table
export default class Language extends Model<Language> {
	@Column({ primaryKey: true, autoIncrement: true })
	code: number;

	@Column
	name: UserLanguage;
}
