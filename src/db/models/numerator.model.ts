import { Table, Column, Model } from 'sequelize-typescript';

@Table
export default class Numerator extends Model<Numerator> {
	@Column({ primaryKey: true, autoIncrement: true })
	id: number;
}
