import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';

import Language from './language.model';
import Numerator from './numerator.model';

@Table
export default class NumeratorTranslation extends Model<NumeratorTranslation> {
	@ForeignKey(() => Language)
	@Column
	languageCode: number;

	@BelongsTo(() => Language)
	language: Language;

	@ForeignKey(() => Numerator)
	@Column({
		primaryKey: true,
	})
	numeratorId: number;

	@BelongsTo(() => Numerator)
	numerator: Numerator;

	@Column
	name: string;
}
