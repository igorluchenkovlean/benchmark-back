import { Table, Column, Model, Default, DataType, ForeignKey, BelongsTo } from 'sequelize-typescript';

import { ExcelDataStatus } from 'typedefs/user';
import Company from './company.model';
import CompanyGroupService from './companyGroupService.model';

@Table
export default class PendingDataRequest extends Model<PendingDataRequest> {
	@Column({ primaryKey: true, autoIncrement: true })
	id: number;

	@ForeignKey(() => Company)
	@Column
	companyId: number;

	@BelongsTo(() => Company)
	company: Company;

	@Column
	status: ExcelDataStatus;

	@ForeignKey(() => CompanyGroupService)
	@Column
	companyGroupServiceId: number;

	@BelongsTo(() => CompanyGroupService)
	companyGroupService: CompanyGroupService;

	@Column
	updatedAt: Date;

	@Default(DataType.NOW)
	@Column
	createdAt: Date;
}
