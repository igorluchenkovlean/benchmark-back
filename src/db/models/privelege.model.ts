import { Table, Column, Model } from 'sequelize-typescript';

@Table
export default class Privelege extends Model<Privelege> {
	@Column({ primaryKey: true, autoIncrement: true })
	id: number;

	@Column
	description: string;
}
