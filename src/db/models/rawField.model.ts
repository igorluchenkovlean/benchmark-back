import { Table, Column, Model } from 'sequelize-typescript';

@Table
export default class RawField extends Model<RawField> {
	@Column({ primaryKey: true, autoIncrement: true })
	id: number;
}
