import { Table, Column, Model, BelongsTo, ForeignKey } from 'sequelize-typescript';

import Language from './language.model';
import RawField from './rawField.model';

@Table
export default class RawFieldTranslation extends Model<RawFieldTranslation> {
	@ForeignKey(() => Language)
	@Column
	languageCode: number;

	@BelongsTo(() => Language)
	language: Language;

	@ForeignKey(() => RawField)
	@Column
	rawFieldId: number;

	@BelongsTo(() => RawField)
	rawField: RawField;

	@Column
	name: string;
}
