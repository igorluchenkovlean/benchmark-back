import { Table, Column, Model } from 'sequelize-typescript';

import { UserRegisterStatus } from 'typedefs/user';

@Table
export default class RegistrationStatus extends Model<RegistrationStatus> {
	@Column({ primaryKey: true, autoIncrement: true })
	id: number;

	@Column
	name: UserRegisterStatus;
}
