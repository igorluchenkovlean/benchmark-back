import { Table, Model, Column, Default, DataType } from 'sequelize-typescript';

@Table
export default class ReservedDomain extends Model<ReservedDomain> {
	@Column({ primaryKey: true, autoIncrement: true })
	id: number;

	@Column
	name: string;

	@Default(DataType.NOW)
	@Column
	createdAt: Date;
}
