import { Table, Column, Model, BelongsTo } from 'sequelize-typescript';

import Privelege from './privelege.model';
import { UserRole } from 'typedefs/user';

@Table
export default class Role extends Model<Role> {
	@Column({ primaryKey: true, autoIncrement: true })
	id: number;

	@Column
	label: UserRole;

	@BelongsTo(() => Privelege, { foreignKey: 'privelegeId', as: 'privelege' })
	privelege: Privelege;
}
