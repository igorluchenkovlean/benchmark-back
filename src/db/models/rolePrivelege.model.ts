import { Table, Model, BelongsTo, ForeignKey, Column } from 'sequelize-typescript';

import Role from './role.model';
import Privelege from './privelege.model';

@Table
export default class RolePrivelege extends Model<RolePrivelege> {
	@ForeignKey(() => Role)
	@Column
	roleId: number;

	@BelongsTo(() => Role)
	role: Role;

	@ForeignKey(() => Privelege)
	@Column
	privelegeId: number;

	@BelongsTo(() => Privelege)
	privelege: Privelege;
}
