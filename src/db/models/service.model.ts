import { Table, Column, Model } from 'sequelize-typescript';

@Table
export default class Service extends Model<Service> {
	@Column({ primaryKey: true, autoIncrement: true })
	id: number;
}
