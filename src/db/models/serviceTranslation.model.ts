import { Table, Column, Model, BelongsTo, ForeignKey, DataType } from 'sequelize-typescript';

import Language from './language.model';
import Service from './service.model';

@Table
export default class ServiceTranslation extends Model<ServiceTranslation> {
	@ForeignKey(() => Language)
	@Column
	languageCode: number;

	@BelongsTo(() => Language)
	language: Language;

	@ForeignKey(() => Service)
	@Column({
		primaryKey: true,
	})
	serviceId: number;

	@BelongsTo(() => Service)
	service: Service;

	@Column
	name: string;

	@Column(DataType.TEXT)
	description: string;
}
