import { Table, Column, Model, DataType, BelongsTo, Default, ForeignKey } from 'sequelize-typescript';

import Language from './language.model';
import Role from './role.model';
import Company from './company.model';

@Table
export default class User extends Model<User> {
	@Column({ primaryKey: true, autoIncrement: true })
	id: number;

	@Column
	firstName: string;

	@Column
	lastName: string;

	@Column(DataType.VIRTUAL)
	get fullName(): string {
		return `${this.getDataValue('firstName')} ${this.getDataValue('lastName')}`;
	}

	@Column
	password: string;

	@Column
	email: string;

	@ForeignKey(() => Language)
	@Column
	languageCode: number;

	@BelongsTo(() => Language)
	language: Language;

	@ForeignKey(() => Role)
	@Column
	roleId: number;

	@BelongsTo(() => Role)
	role: Role;

	@ForeignKey(() => Company)
	@Column
	companyId: number;

	@BelongsTo(() => Company)
	company: Company;

	@Default(DataType.NOW)
	@Column
	createdAt: Date;
}
