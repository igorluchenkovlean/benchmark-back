import { Table, Column, Model, BelongsTo, ForeignKey } from 'sequelize-typescript';

import User from './user.model';
import Group from './group.model';

@Table
export default class UserGroup extends Model<UserGroup> {
	@Column({ primaryKey: true, autoIncrement: true })
	id: number;

	@ForeignKey(() => User)
	@Column
	userId: number;

	@BelongsTo(() => User)
	user: User;

	@ForeignKey(() => Group)
	@Column
	groupId: number;

	@BelongsTo(() => Group)
	group: Group;
}
