import { Table, Model, BelongsTo, ForeignKey, Column } from 'sequelize-typescript';

import Service from './serviceTranslation.model';
import UserGroup from './userGroup.model';

@Table
export default class UserGroupService extends Model<UserGroupService> {
	@ForeignKey(() => UserGroup)
	@Column
	userGroupId: number;

	@BelongsTo(() => UserGroup)
	userGroup: UserGroup;

	@ForeignKey(() => Service)
	@Column
	serviceId: number;

	@BelongsTo(() => Service)
	service: Service;
}
