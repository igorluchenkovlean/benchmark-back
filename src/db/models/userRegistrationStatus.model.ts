import { Table, Model, BelongsTo, ForeignKey, Column, Default, DataType } from 'sequelize-typescript';

import User from './user.model';
import RegistrationStatus from './registrationStatus.model';
import ReservedDomain from './reservedDomain.model';

@Table
export default class UserRegistrationStatus extends Model<UserRegistrationStatus> {
	@ForeignKey(() => User)
	@Column
	userId: number;

	@BelongsTo(() => User)
	user: User;

	@ForeignKey(() => RegistrationStatus)
	@Column
	registrationStatusId: number;

	@BelongsTo(() => RegistrationStatus)
	registrationStatus: RegistrationStatus;

	@ForeignKey(() => ReservedDomain)
	@Column
	reservedDomainId: number;

	@BelongsTo(() => ReservedDomain)
	reservedDomain: ReservedDomain;

	@Column
	updatedAt: Date;

	@Default(DataType.NOW)
	@Column
	createdAt: Date;

	@Column
	token: string;
}
