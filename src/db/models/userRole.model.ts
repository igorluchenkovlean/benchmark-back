import { Table, Column, Model, BelongsTo, ForeignKey, Default, DataType } from 'sequelize-typescript';

import User from './user.model';
import Role from './role.model';

@Table
export default class UserRole extends Model<UserRole> {
	@ForeignKey(() => User)
	@Column
	userId: number;

	@BelongsTo(() => User)
	user: User;

	@ForeignKey(() => Role)
	@Column
	roleId: number;

	@BelongsTo(() => Role)
	role: Role;

	@Default(DataType.NOW)
	@Column
	createdAt: Date;
}
