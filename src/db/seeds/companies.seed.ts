import { Sequelize } from 'sequelize';

export const companiesSeed = async (db: Sequelize) =>
	db.models.Company.bulkCreate(
		[
			{
				name: 'CompanyA',
				industryId: 1,
				country: 'Russia',
				employeesNumber: 500,
				city: 'Moscow',
				postalCode: '345454',
			},
			{
				name: 'CompanyB',
				industryId: 2,
				country: 'Russia',
				employeesNumber: 200,
				city: 'Moscow',
				postalCode: '345454',
			},
			{
				name: 'CompanyC',
				industryId: 3,
				country: 'Russia',
				employeesNumber: 200,
				city: 'Moscow',
				postalCode: '345454',
			},
			{
				name: 'CompanyD',
				industryId: 4,
				country: 'Russia',
				employeesNumber: 200,
				city: 'Moscow',
				postalCode: '345454',
			},
			{
				name: 'CompanyE',
				industryId: 5,
				country: 'Russia',
				employeesNumber: 200,
				city: 'Moscow',
				postalCode: '345454',
			},
			{
				name: 'CompanyF',
				industryId: 6,
				country: 'Russia',
				employeesNumber: 200,
				city: 'Moscow',
				postalCode: '345454',
			},
			{
				name: 'CompanyH',
				industryId: 7,
				country: 'Russia',
				employeesNumber: 200,
				city: 'Moscow',
				postalCode: '345454',
			},
			{
				name: 'CompanyI',
				industryId: 8,
				country: 'Russia',
				employeesNumber: 200,
				city: 'Moscow',
				postalCode: '345454',
			},
			{
				name: 'CompanyJ',
				industryId: 9,
			},
			{
				name: 'CompanyG',
				industryId: 10,
			},
			{
				name: 'CompanyL',
				industryId: 11,
				country: 'Russia',
				employeesNumber: 200,
				city: 'Moscow',
				postalCode: '345454',
			},
			{
				name: 'CompanyP',
				industryId: 12,
				country: 'Russia',
				employeesNumber: 200,
				city: 'Moscow',
				postalCode: '345454',
			},
		],
		{
			logging: false,
		},
	);
