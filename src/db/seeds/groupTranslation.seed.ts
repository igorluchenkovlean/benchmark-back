import { Sequelize } from 'sequelize';

import { groupData } from './utils/groupData';

export const groupTranslationsSeed = (db: Sequelize) =>
	db.models.GroupTranslation.bulkCreate(groupData, {
		logging: false,
	});
