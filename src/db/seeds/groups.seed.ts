import { Sequelize } from 'sequelize';

import { groupData } from './utils/groupData';

export const groupsSeed = async (db: Sequelize) =>
	Promise.all(
		groupData.map(async () => {
			return db.models.Group.create();
		}),
	);
