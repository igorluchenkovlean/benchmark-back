import { Sequelize } from 'sequelize/types';

import { usersSeed } from './users.seed';
import { languagesSeed } from './languages.seed';
import { privelegesSeed } from './privelege.seed';
import { rolesSeed } from './role.seed';
import { rolePrivelegesSeed } from './rolePriveleges.seed';
import { companiesSeed } from './companies.seed';
import { industriesSeed } from './industries.seed';
import { userRoleSeed } from './userRole.seed';
import { industryTranslationSeed } from './industryTranslation.seed';
import { registrationStatusSeed } from './registrationStatus.seed';
import { userRegistrationStatusSeed } from './userRegistrationStatus.seed';
import { reservedDomainSeed } from './reservedDomains.seed';
import { servicesSeed } from './services.seed';
import { servicesTranslationsSeed } from './servicesTranslations.seed';
import { groupsSeed } from './groups.seed';
import { groupTranslationsSeed } from './groupTranslation.seed';

export const makeSeeds = async (db: Sequelize) => {
	console.log(`🚀 Seeds are starting`);
	await languagesSeed(db);
	await privelegesSeed(db);
	await rolesSeed(db);
	await rolePrivelegesSeed(db);
	await industriesSeed(db);
	await industryTranslationSeed(db);
	await companiesSeed(db);
	await usersSeed(db);
	await userRoleSeed(db);
	await reservedDomainSeed(db);
	await registrationStatusSeed(db);
	await userRegistrationStatusSeed(db);
	await servicesSeed(db);
	await servicesTranslationsSeed(db);
	await groupsSeed(db);
	await groupTranslationsSeed(db);
	console.log(`🚀 Seeds are done`);

	return db;
};
