import { Sequelize } from 'sequelize';

export const industriesSeed = async (db: Sequelize) => {
	for (let i = 0; i < 12; i++) {
		await db.models.Industry.create();
	}
};
