import { Sequelize } from 'sequelize';

export const industryTranslationSeed = async (db: Sequelize) =>
	db.models.IndustryTranslation.bulkCreate(
		[
			{
				name: 'testA',
				industryId: 1,
				languageCode: 1,
			},
			{
				name: 'testB',
				industryId: 2,
				languageCode: 1,
			},
			{
				name: 'testC',
				industryId: 3,
				languageCode: 1,
			},
			{
				name: 'testD',
				industryId: 4,
				languageCode: 1,
			},
			{
				name: 'testE',
				industryId: 5,
				languageCode: 1,
			},
			{
				name: 'testF',
				industryId: 6,
				languageCode: 1,
			},
			{
				name: 'testJ',
				industryId: 7,
				languageCode: 1,
			},
			{
				name: 'testK',
				industryId: 8,
				languageCode: 1,
			},
			{
				name: 'testO',
				industryId: 9,
				languageCode: 1,
			},
			{
				name: 'testP',
				industryId: 10,
				languageCode: 1,
			},
			{
				name: 'testM',
				industryId: 11,
				languageCode: 1,
			},
			{
				name: 'testN',
				industryId: 12,
				languageCode: 1,
			},
		],
		{
			logging: false,
		},
	);
