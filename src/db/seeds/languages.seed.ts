import { Sequelize } from 'sequelize';

import { UserLanguage } from 'typedefs/user';

export const languagesSeed = async (db: Sequelize) =>
	db.models.Language.bulkCreate(
		[
			{
				name: UserLanguage.en,
			},
			{
				name: UserLanguage.ge,
			},
		],
		{
			logging: false,
		},
	);
