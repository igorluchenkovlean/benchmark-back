import { Sequelize } from 'sequelize';

export const privelegesSeed = async (db: Sequelize) =>
	db.models.Privelege.bulkCreate(
		[
			{
				description: 'high',
			},
			{
				description: 'medium',
			},
		],
		{
			logging: false,
		},
	);
