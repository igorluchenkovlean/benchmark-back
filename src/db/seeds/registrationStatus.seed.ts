import { Sequelize } from 'sequelize';

import { UserRegisterStatus } from 'typedefs/user';

export const registrationStatusSeed = async (db: Sequelize) =>
	db.models.RegistrationStatus.bulkCreate(
		[
			{
				name: UserRegisterStatus.completed,
			},
			{
				name: UserRegisterStatus.completed,
			},
			{
				name: UserRegisterStatus.completed,
			},
			{
				name: UserRegisterStatus.completed,
			},
			{
				name: UserRegisterStatus.completed,
			},
			{
				name: UserRegisterStatus.completed,
			},
			{
				name: UserRegisterStatus.completed,
			},
			{
				name: UserRegisterStatus.pending,
			},
			{
				name: UserRegisterStatus.pending,
			},
			{
				name: UserRegisterStatus.pending,
			},
			{
				name: UserRegisterStatus.removed,
			},
			{
				name: UserRegisterStatus.removed,
			},
		],
		{
			logging: false,
		},
	);
