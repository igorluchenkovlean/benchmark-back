import { Sequelize } from 'sequelize';

export const reservedDomainSeed = async (db: Sequelize) =>
	db.models.ReservedDomain.bulkCreate(
		[
			{
				name: 'DomainA',
			},
			{
				name: 'DomainB',
			},
			{
				name: 'DomainC',
			},
			{
				name: 'DomainD',
			},
			{
				name: 'DomainE',
			},
			{
				name: 'DomainF',
			},
			{
				name: 'DomainH',
			},
			{
				name: 'DomainR',
			},
			{
				name: 'DomainT',
			},
			{
				name: 'DomainY',
			},
			{
				name: 'DomainU',
			},
			{
				name: 'DomainI',
			},
		],
		{
			logging: false,
		},
	);
