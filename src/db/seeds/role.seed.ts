import { Sequelize } from 'sequelize';

import { UserRole } from 'typedefs/user';

export const rolesSeed = async (db: Sequelize) =>
	db.models.Role.bulkCreate(
		[
			{
				label: UserRole.superAdmin,
				privelegeId: 1,
			},
			{
				label: UserRole.producer,
				privelegeId: 2,
			},
		],
		{
			logging: false,
		},
	);
