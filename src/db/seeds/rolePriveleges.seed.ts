import { Sequelize } from 'sequelize';

export const rolePrivelegesSeed = async (db: Sequelize) =>
	db.models.RolePrivelege.bulkCreate(
		[
			{
				privelegeId: 1,
				roleId: 1,
			},
			{
				privelegeId: 2,
				roleId: 2,
			},
		],
		{
			logging: false,
		},
	);
