import { Sequelize } from 'sequelize';

import { servicesData } from './utils/servicesData';

export const servicesSeed = async (db: Sequelize) =>
	Promise.all(
		servicesData.map(async () => {
			return db.models.Service.create();
		}),
	);
