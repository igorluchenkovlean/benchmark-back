import { Sequelize } from 'sequelize';

import { servicesData } from './utils/servicesData';

export const servicesTranslationsSeed = (db: Sequelize) =>
	db.models.ServiceTranslation.bulkCreate(servicesData, {
		logging: false,
	});
