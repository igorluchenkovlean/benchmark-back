import { Sequelize } from 'sequelize';

export const userRegistrationStatusSeed = async (db: Sequelize) =>
	db.models.UserRegistrationStatus.bulkCreate(
		[
			{
				userId: 1,
				registrationStatusId: 1,
				token: '324324sadsad433454345rt32432asdczxc',
				updatedAt: new Date(),
				reservedDomainId: 1,
			},
			{
				userId: 2,
				registrationStatusId: 2,
				token: '32dfdsfcxvxcv4324sadsad433454345rtf',
				updatedAt: new Date(),
				reservedDomainId: 2,
			},
			{
				userId: 3,
				registrationStatusId: 3,
				token: '32dfdsfcxvxcv4324sadsad433454345rtf',
				updatedAt: new Date(),
				reservedDomainId: 3,
			},
			{
				userId: 4,
				registrationStatusId: 4,
				token: '32dfdsfcxvxcv4324sadsad433454345rtf',
				updatedAt: new Date(),
				reservedDomainId: 4,
			},
			{
				userId: 5,
				registrationStatusId: 5,
				token: '32dfdsfcxvxcv4324sadsad433454345rtf',
				updatedAt: new Date(),
				reservedDomainId: 5,
			},
			{
				userId: 6,
				registrationStatusId: 6,
				token: '32dfdsfcxvxcv4324sadsad433454345rtf',
				updatedAt: new Date(),
				reservedDomainId: 6,
			},
			{
				userId: 7,
				registrationStatusId: 7,
				token: '32dfdsfcxvxcv4324sadsad433454345rtf',
				updatedAt: new Date(),
				reservedDomainId: 7,
			},
			{
				userId: 8,
				registrationStatusId: 8,
				token: '32dfdsfcxvxcv4324sadsad433454345rtf',
				updatedAt: new Date(),
				reservedDomainId: 8,
			},
			{
				userId: 9,
				registrationStatusId: 9,
				token: '32dfdsfcxvxcv4324sadsad433454345rtf',
				updatedAt: new Date(),
				reservedDomainId: 9,
			},
			{
				userId: 10,
				registrationStatusId: 10,
				token: '32dfdsfcxvxcv4324sadsad433454345rtf',
				updatedAt: new Date(),
				reservedDomainId: 10,
			},
			{
				userId: 11,
				registrationStatusId: 11,
				token: '32dfdsfcxvxcv4324sadsad433454345rtf',
				updatedAt: new Date(),
				reservedDomainId: 11,
			},
			{
				userId: 12,
				registrationStatusId: 12,
				token: '32dfdsfcxvxcv4324sadsad433454345rtf',
				updatedAt: new Date(),
				reservedDomainId: 12,
			},
		],
		{
			logging: false,
		},
	);
