import { Sequelize } from 'sequelize';

export const userRoleSeed = async (db: Sequelize) =>
	db.models.UserRole.bulkCreate(
		[
			{
				userId: 1,
				roleId: 1,
			},
			{
				userId: 2,
				roleId: 2,
			},
			{
				userId: 2,
				roleId: 2,
			},
			{
				userId: 2,
				roleId: 2,
			},
			{
				userId: 2,
				roleId: 2,
			},

			{
				userId: 2,
				roleId: 2,
			},
			{
				userId: 2,
				roleId: 2,
			},
			{
				userId: 2,
				roleId: 2,
			},
			{
				userId: 2,
				roleId: 2,
			},
			{
				userId: 2,
				roleId: 2,
			},

			{
				userId: 2,
				roleId: 2,
			},
			{
				userId: 2,
				roleId: 2,
			},
		],
		{
			logging: false,
		},
	);
