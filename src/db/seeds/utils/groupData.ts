export const groupData = [
	{ groupId: 1, languageCode: 1, name: 'Health care' },
	{ groupId: 2, languageCode: 1, name: 'Site security & protection' },
	{ groupId: 3, languageCode: 1, name: 'General security' },
	{ groupId: 4, languageCode: 1, name: 'Health care & general security' },
	{ groupId: 5, languageCode: 1, name: 'Fire brigade' },
	{ groupId: 6, languageCode: 1, name: 'Fleet management' },
	{ groupId: 7, languageCode: 1, name: 'Logistics' },
	{ groupId: 8, languageCode: 1, name: 'Area & Building Management' },
	{ groupId: 9, languageCode: 1, name: 'CREM - Area' },
	{ groupId: 10, languageCode: 1, name: 'Infrastructure Facility Management' },
	{ groupId: 11, languageCode: 1, name: 'Facility Management' },
	{ groupId: 12, languageCode: 1, name: 'Real Estate Management' },
	{ groupId: 13, languageCode: 1, name: 'Site infrastructure' },
	{ groupId: 14, languageCode: 1, name: 'Catering' },
	{ groupId: 15, languageCode: 1, name: 'Management & strategy' },
	{ groupId: 16, languageCode: 1, name: 'Site management' },
	{ groupId: 17, languageCode: 1, name: 'Supply' },
	{ groupId: 18, languageCode: 1, name: 'Disposal' },
	{ groupId: 19, languageCode: 1, name: 'Sewage treatment' },
	{ groupId: 20, languageCode: 1, name: 'Supply & disposal' },
	{ groupId: 21, languageCode: 1, name: 'Engineering / Technical services' },
	{ groupId: 22, languageCode: 1, name: 'Technical services' },
	{ groupId: 23, languageCode: 1, name: 'Idea management' },
];
