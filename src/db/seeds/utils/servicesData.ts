export const servicesData = [
	{
		languageCode: 1,
		serviceId: 1,
		name: 'Medical service',
		description: 'Occupational health advice and assessment',
	},
	{
		languageCode: 1,
		serviceId: 2,
		name: 'Ergonomics',
		description: 'Advice, assessment and measures taken to align working conditions with ergonomic requirements',
	},
	{
		languageCode: 1,
		serviceId: 3,
		name: 'Health Care',
		description: 'Measures to maintain health or/or prevent possible diseases',
	},
	{
		languageCode: 1,
		serviceId: 4,
		name: 'Occupational health care',
		description: 'Medical analysis, evaluation and design of work activities and conditions',
	},
	{
		languageCode: 1,
		serviceId: 5,
		name: 'Occupational Psychological Service',
		description: 'Psychological analysis, evaluation and design of work activities and conditions',
	},
	{
		languageCode: 1,
		serviceId: 6,
		name: 'Worksplace Hygiene (Biomonitoring)',
		description:
			'Analysis, evaluation and design of work activities and conditions taking into account the exposure of employees to pollutants',
	},
	{
		languageCode: 1,
		serviceId: 7,
		name: 'Emergency medical care',
		description: 'Detection and treatment of medical emergencies caused by accident or illness',
	},
	{
		languageCode: 1,
		serviceId: 8,
		name: 'Communication occupational safety',
		description: 'Communication of measures taken to control and minimise risks to the safety and health of workers',
	},
	{
		languageCode: 1,
		serviceId: 9,
		name: 'Occupational and contractor safety',
		description:
			'Measures taken to prevent accidents at work and work-related health hazards, including the appropriate design of workplaces and conditions (including for third parties such as suppliers, subcontractors, etc.)',
	},
	{
		languageCode: 1,
		serviceId: 10,
		name: 'Other services health care & general security',
		description: null,
	},
	{
		languageCode: 1,
		serviceId: 11,
		name: 'Hardware site access',
		description:
			'Creation, operation, maintenance and securing of the physical limitations of all areas belonging to the site, the infrastructure and systems for access control, key management and identification office as well as the infrastructure for the site access of cars and trucks (all infrastructure related to site access including physical external borders, separation systems, cameras, etc.)',
	},
	{
		languageCode: 1,
		serviceId: 12,
		name: 'Site Security',
		description: 'Management and organisation of all site security measures (personnel costs of site security)',
	},
	{
		languageCode: 1,
		serviceId: 13,
		name: 'Other services site security & protection',
		description: null,
	},
	{
		languageCode: 1,
		serviceId: 14,
		name: 'Fire brigade - Optional services',
		description:
			'Services of the fire brigade outside of emergencies usually at the instigation of the site management or user',
	},
	{
		languageCode: 1,
		serviceId: 15,
		name: 'Fire brigade - Fire protection',
		description:
			'All fire brigade operations in the event of fires, accidents, floods or other events or emergencies (incl. false alarms)',
	},
	{
		languageCode: 1,
		serviceId: 16,
		name: 'Firewater Grid',
		description: 'Development, operation, maintenance of water supply systems for fire protection',
	},
	{
		languageCode: 1,
		serviceId: 17,
		name: 'Other services fire brigade',
		description: null,
	},
	{
		languageCode: 1,
		serviceId: 18,
		name: 'Car workshop',
		description: 'Installation, operation and maintenance of repair shops for motor vehicles',
	},
	{
		languageCode: 1,
		serviceId: 19,
		name: 'Forklift workshop',
		description:
			'Installation, operation and maintenance of repair shops for forklifts and other lifting and lifting vehicles',
	},
	{
		languageCode: 1,
		serviceId: 20,
		name: 'Management of company bicycles',
		description:
			'Operation of facilities for receiving goods of any kind, such as letters, parcels or pallets (excl. production raw materials)',
	},
	{
		languageCode: 1,
		serviceId: 21,
		name: 'Leasing and fleet management',
		description:
			'Managing, planning, controlling, monitoring and financing of vehicle fleet; provision of vehicles to employees; Processing of repair, insurance and warranty cases as well as tyre changes',
	},
	{
		languageCode: 1,
		serviceId: 22,
		name: 'Central acceptance of materials',
		description:
			'Operation of facilities for receiving goods of any kind, such as letters, parcels or pallets (excl. production raw materials)',
	},
	{
		languageCode: 1,
		serviceId: 23,
		name: 'Postal service',
		description: 'Management, collection and distribution of post within the site',
	},
	{
		languageCode: 1,
		serviceId: 24,
		name: 'Logistics (other material transports)',
		description: 'All other transports of materials within the site (excluding postal and production raw materials)',
	},
	{
		languageCode: 1,
		serviceId: 25,
		name: 'Passenger transport by bus  (within the site)',
		description: 'Transports of persons by bus within the site (e.g. visitor trips)',
	},
	{
		languageCode: 1,
		serviceId: 26,
		name: 'Inventory management (storage & management)',
		description:
			'Installation, operation and maintenance of warehouses and magazines (excl. solvent storage); Management and stock management',
	},
	{
		languageCode: 1,
		serviceId: 27,
		name: 'Other services logistics',
		description: null,
	},
	{
		languageCode: 1,
		serviceId: 28,
		name: 'Space management (buildings)',
		description: 'All planning activities in connection with land occupancy and land development within the site',
	},
	{
		languageCode: 1,
		serviceId: 29,
		name: 'Area management (location)',
		description: 'All planning activities in connection with land occupancy and land development within the site',
	},
	{
		languageCode: 1,
		serviceId: 30,
		name: 'New customer management',
		description:
			'Search for, extraction and settlement of businesses or companies at the site; care for settlers; Distribution, handling of the supply and billing of infrastructure and services',
	},
	{
		languageCode: 1,
		serviceId: 31,
		name: 'Workplace management',
		description:
			'Planning, provision and maintenance of workplaces and workplace-relevant environments (including relocation); development of workplaces related, e.g. to ergonomics, work motivation, communication flow, occupational safety, etc.',
	},
	{
		languageCode: 1,
		serviceId: 32,
		name: 'Real Estate Management',
		description:
			'Success- and value-oriented procurement, management, development and marketing of real estate on site',
	},
	{
		languageCode: 1,
		serviceId: 33,
		name: 'Cleaning & laundry Ssvice',
		description:
			'Dry cleaning of non-washable textiles; organisation of the washing or reprocessing of washable textiles (professional clothing); entire cleaning performance',
	},
	{
		languageCode: 1,
		serviceId: 34,
		name: 'Roads and streetlights',
		description:
			'The installation, operation and maintenance of roads, paths and squares and their lighting; Planning and implementation of the winter service/ cleaning',
	},
	{
		languageCode: 1,
		serviceId: 35,
		name: 'Greening and landscape maintenance',
		description:
			'Planning and implementation of all measures for the maintenance and conservation of green spaces on site',
	},
	{
		languageCode: 1,
		serviceId: 36,
		name: 'Parking spaces',
		description:
			'Planning and designation of the areas for parking spaces; If applicable, billing of usage fees and operation and maintenance required for access control and fees facilities (all employee parking spaces on- and off-site), incl. additional services (without consumption costs, e.g. for e-charging stations)',
	},
	{
		languageCode: 1,
		serviceId: 37,
		name: 'Central conference service',
		description:
			'Preparation, implementation and follow-up of conferences: Areas and rooms, seating and room equipment, etc.; Excl. catering',
	},
	{
		languageCode: 1,
		serviceId: 38,
		name: 'Building management',
		description:
			'Operational deployment and maintenance of office buildings (incl. building maintenance cleaning) with laboratory share max. 30%',
	},
	{
		languageCode: 1,
		serviceId: 39,
		name: 'Other services Real Estate Management',
		description: null,
	},
	{
		languageCode: 1,
		serviceId: 40,
		name: 'Customer service incl. Ticketing',
		description:
			'Customer support: Receipt of fault reports and full service for all buildings incl. TGA, outdoor facilities and areas, building envelope of production plants (excluding production facilities)',
	},
	{
		languageCode: 1,
		serviceId: 41,
		name: 'Central dokument service',
		description:
			'Planning, stockpiling and distribution of printed and copymaterial; Central printing and copying centre on site',
	},
	{
		languageCode: 1,
		serviceId: 42,
		name: 'Archives',
		description: 'Establishment and operation of documentation equipment, cabinets or archives',
	},
	{
		languageCode: 1,
		serviceId: 43,
		name: 'Pipeline bridges',
		description: 'Construction, operation and maintenance of pipeline bridges',
	},
	{
		languageCode: 1,
		serviceId: 44,
		name: 'Other infrastructure, such as pipes, networks, etc.',
		description: 'Construction, operation and maintenance of other infrastructure (fibre and telephone networks)',
	},
	{
		languageCode: 1,
		serviceId: 45,
		name: 'Canteen infrastructure',
		description:
			'The establishment and provision of spaces for canteens and other catering establishments; Excl. floor kitchens',
	},
	{
		languageCode: 1,
		serviceId: 46,
		name: 'Other services site infrastructure',
		description: null,
	},
	{
		languageCode: 1,
		serviceId: 47,
		name: 'Site communication (internal)',
		description:
			'Planning and implementation of site-related communication incl. information events for people who move around the site',
	},
	{
		languageCode: 1,
		serviceId: 48,
		name: 'Site communication (external)',
		description:
			'Planning and implementation of site-related communication incl. information events for persons who move outside the site, e.g. residents, municipalities, the press; Without communication purchasing/ tenders, sponsorship, investor-related communication',
	},
	{
		languageCode: 1,
		serviceId: 49,
		name: 'Centre for Visitors',
		description: 'Exhibition incl. costs of the premises',
	},
	{
		languageCode: 1,
		serviceId: 50,
		name: 'Other services site management',
		description: null,
	},
	{
		languageCode: 1,
		serviceId: 51,
		name: 'Electricity trade',
		description: 'Purchasing and sale of energy (exchange',
	},
	{
		languageCode: 1,
		serviceId: 52,
		name: 'Electricity - Generation',
		description: 'Construction, operation and maintenance of all power generation facilities',
	},
	{
		languageCode: 1,
		serviceId: 53,
		name: 'Electricity - Networks',
		description:
			'The construction, operation and maintenance of all power distribution facilities; Viewing > 400V network (without building [a=400V network])',
	},
	{
		languageCode: 1,
		serviceId: 54,
		name: 'Steam - Generation',
		description: 'Construction, operation and maintenance of all steam generation facilities',
	},
	{
		languageCode: 1,
		serviceId: 55,
		name: 'Steam - Networks',
		description: 'Construction, operation and maintenance of all steam distribution facilities',
	},
	{
		languageCode: 1,
		serviceId: 56,
		name: 'Gas - Networks',
		description: 'The construction, operation and maintenance of all gas distribution facilities; excl. natural gas',
	},
	{
		languageCode: 1,
		serviceId: 57,
		name: 'Compressed air - Generation',
		description: 'Construction, operation and maintenance of all compressed air production facilities',
	},
	{
		languageCode: 1,
		serviceId: 58,
		name: 'Compressed air - Networks',
		description: 'Construction, operation and maintenance of all compressed air distribution facilities',
	},
	{
		languageCode: 1,
		serviceId: 59,
		name: 'Cold - Generation ',
		description: 'Construction, operation and maintenance of all refrigeration facilities',
	},
	{
		languageCode: 1,
		serviceId: 60,
		name: 'Cold - Networks ',
		description: 'Construction, operation and maintenance of all refrigeration distribution facilities',
	},
	{
		languageCode: 1,
		serviceId: 61,
		name: 'Drinking water - Production/ procurement',
		description: 'Construction, operation and maintenance of all facilities for the supply of drinking water',
	},
	{
		languageCode: 1,
		serviceId: 62,
		name: 'Drinking water - Networks',
		description: 'Construction, operation and maintenance of all facilities for drinking water distribution',
	},
	{
		languageCode: 1,
		serviceId: 63,
		name: 'Drinkingwater - Networks',
		description: 'Construction, operation and maintenance of all facilities for drinking water distribution',
	},
	{
		languageCode: 1,
		serviceId: 64,
		name: 'Operating water - Production/ procurement',
		description: 'Construction, operation and maintenance of all facilities for the procurement of operating water',
	},
	{
		languageCode: 1,
		serviceId: 65,
		name: 'Operating water - Networks',
		description: 'Construction, operation and maintenance of all operating water distribution facilities',
	},
	{
		languageCode: 1,
		serviceId: 66,
		name: 'DI-Water - Production/ procurement',
		description: 'Construction, operation and maintenance of all facilities for DI-water procurement',
	},
	{
		languageCode: 1,
		serviceId: 67,
		name: 'DI-Water - Networks',
		description: 'Construction, operation and maintenance of all facilities for DI-water distribution',
	},
	{
		languageCode: 1,
		serviceId: 68,
		name: 'Extinguishing water network',
		description: 'Creation, operation, maintenance of water supply systems for fire protection',
	},
	{
		languageCode: 1,
		serviceId: 69,
		name: 'Energy billing',
		description:
			'Personnel costs for the billing of energy/ media to users; excl. maintenance (= for individual networks); excl. purchasing',
	},
	{
		languageCode: 1,
		serviceId: 70,
		name: 'Energy management',
		description: 'All charged media (electricity, steam, etc...)',
	},
	{
		languageCode: 1,
		serviceId: 71,
		name: 'Waste management',
		description:
			'Procurement coordination of disposal services; planning and coordination of all measures to avoid, reduce, recycle and dispose of waste (excluding waste used as raw material); Assignment of waste types in the appropriate places',
	},
	{
		languageCode: 1,
		serviceId: 72,
		name: 'Waste management',
		description: 'Implementation of small projects and maintenance measures for users with a volume of < € 25',
	},
	{
		languageCode: 1,
		serviceId: 73,
		name: 'Central sewage treatment plant',
		description:
			'Implementation of small projects and maintenance measures for on-site infrastructure with a volume of < € 25',
	},
	{
		languageCode: 1,
		serviceId: 74,
		name: 'Highly polluted waste water - Networks',
		description:
			'Construction, operation and maintenance of all facilities for the disposal of highly polluted waste water',
	},
	{
		languageCode: 1,
		serviceId: 75,
		name: 'Low-polluted waste water - Networks',
		description:
			'Construction, operation and maintenance of all facilities for the disposal of low-polluted waste water',
	},
	{
		languageCode: 1,
		serviceId: 76,
		name: 'Unpolluted surface water - Networks',
		description:
			'Construction, operation and maintenance of all facilities for the distribution of unpolluted surface water',
	},
	{
		languageCode: 1,
		serviceId: 77,
		name: 'Other services supply & disposal',
		description: null,
	},
	{
		languageCode: 1,
		serviceId: 78,
		name: 'Small projects and technical measures for users on site (as the user has commissioned)',
		description: 'Implementation of small projects and maintenance measures for users with a volume of < € 25',
	},
	{
		languageCode: 1,
		serviceId: 79,
		name:
			'Small projects and technical measures for the infrastructure on site (currently commissioned by site management)',
		description:
			'Implementation of small projects and maintenance measures for on-site infrastructure with a volume of < € 25',
	},
	{
		languageCode: 1,
		serviceId: 80,
		name: 'Projects for users on site (usually commissioned by the user)',
		description:
			'Implementation of projects and technical measures for users with a volume of < € 3 - 5 million per individual measure (CAPEX)',
	},
	{
		languageCode: 1,
		serviceId: 81,
		name: 'Projects for the infrastructure on site (usually commissioned by site management)',
		description:
			'Implementation of projects and technical measures for users with a volume of < € 3 - 5 million per individual measure (CAPEX)',
	},
	{
		languageCode: 1,
		serviceId: 82,
		name: 'Large-scale projects for users on site (usually commissioned by the user',
		description:
			'Implementation of projects and technical measures for users with a volume > € 3 - 5 million per individual measure (CAPEX)',
	},
	{
		languageCode: 1,
		serviceId: 83,
		name: 'Large-scale projects for the infrastructure on site (usually commissioned by the site management)',
		description:
			'Implementation of projects and technical measures for users with a volume > € 3 - 5 million per individual measure (CAPEX)',
	},
	{
		languageCode: 1,
		serviceId: 84,
		name: 'Projects and measures for users and infrastructure on site',
		description:
			'Implementation of projects and technical measures for users and infrastructure (sum of services #67 - #72)',
	},
	{
		languageCode: 1,
		serviceId: 85,
		name: 'Procedural security',
		description:
			'All engineering measures taken to ensure the safety of operational processes and processes (e.g. pressure testing)',
	},
	{
		languageCode: 1,
		serviceId: 86,
		name: 'Other services technical services',
		description: null,
	},
	{
		languageCode: 1,
		serviceId: 87,
		name: 'Idea management',
		description: 'Costs for idea management',
	},
	{
		languageCode: 1,
		serviceId: 88,
		name: 'Other services idea management',
		description: null,
	},
];
