import { AuthenticationError } from 'apollo-server';
import { verify } from 'jsonwebtoken';
import { Request, Response } from 'express';
import { ExecutionParams } from 'subscriptions-transport-ws';

import db, { models } from 'db';
import { UserInJWT } from 'typedefs/user';
import User from 'db/models/user.model';

export type Context = Readonly<{
	req: Express.Request;
	res: Express.Response;
	connection: ExecutionParams;
	models: typeof models;
	user: User;
}>;

export type UnauthContext = Omit<Context, 'user'>;

declare namespace Express {
	export interface Request {
		ip: string;
		protocol: string;
		headers: {
			[x: string]: string | string[] | undefined;
		};
		host: string;
	}
	export interface Response {
		set: (name: string, value: string | number) => void;
		cookie: (name: string, value: string | number, option: { maxAge: number; httpOnly: boolean }) => void;
	}
}

export const context = async ({
	req,
	res,
	connection,
}: {
	req: Request;
	res: Response;
	connection: ExecutionParams;
}): Promise<Context | UnauthContext> => {
	try {
		const user = await getUserFromReq(req || { headers: { authorization: connection.context.Authorization } });
		return { req, res, models, user, connection };
	} catch (e) {
		throw new AuthenticationError('Token is incorrect');
	}
};

async function getUserFromReq(req: Request) {
	const [, token] = (req.headers.authorization || '').split(' ');
	if (token) {
		const { id } = verify(token, process.env.JWT_SECRET_KEY as string) as UserInJWT;
		if (id) {
			const user = (await db.models.User.findByPk(id)) as User;
			if (user) {
				return user;
			}
		}
	}

	return undefined;
}
