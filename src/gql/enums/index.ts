import { readdirSync } from 'fs';
import { basename } from 'path';

const base = basename(__filename);
readdirSync(__dirname)
	.filter((file: string) => {
		return file.indexOf('.') !== 0 && file !== base && (file.slice(-3) === '.ts' || file.slice(-3) === '.js');
	})
	.forEach((file: string) => require('./' + file));
