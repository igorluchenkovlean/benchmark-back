import { registerEnumType } from 'type-graphql';

import { UserRole, UserLanguage, UserRegisterStatus, ExcelDataStatus } from 'typedefs/user';

registerEnumType(UserRole, {
	name: 'UserRole',
});

registerEnumType(UserLanguage, {
	name: 'UserLanguage',
});

registerEnumType(UserRegisterStatus, {
	name: 'UserRegisterStatus',
});

registerEnumType(ExcelDataStatus, {
	name: 'PendingDataRequest',
});
