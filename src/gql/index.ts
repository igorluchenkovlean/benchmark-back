import { buildSchema } from 'type-graphql';

export { context } from './context';

import './enums';
import { resolvers } from './resolvers';

export const createSchema = () =>
	buildSchema({
		resolvers,
		validate: false,
	});
