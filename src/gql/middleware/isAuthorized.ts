import { MiddlewareFn } from 'type-graphql';
import { AuthenticationError } from 'apollo-server';

import { Context } from 'gql/context';

export const isAuthorized: MiddlewareFn<Context> = async ({ context }, next) => {
	if (context.connection) {
		return next();
	}

	if (!context.user) throw new AuthenticationError('Not authorized');

	return next();
};
