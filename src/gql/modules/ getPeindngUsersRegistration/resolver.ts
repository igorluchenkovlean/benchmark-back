import { Resolver, Ctx, UseMiddleware, Query } from 'type-graphql';
import { col } from 'sequelize';

import { Context } from 'gql/context';
import { isAuthorized } from 'gql/middleware';
import { PendingUsersCompanies } from 'gql/types';

@Resolver()
export class GetPeindngUsersRegistrationResolver {
	@UseMiddleware(isAuthorized)
	@Query(() => [PendingUsersCompanies])
	async getPeindngUsersRegistration(@Ctx() ctx: Context) {
		try {
			return ctx.models.UserRegistrationStatus.findAll({
				raw: true,
				where: {
					['$registrationStatus.name$']: 'pending',
					['$user.roleId$']: 2,
				},
				subQuery: false,
				include: [
					{
						model: ctx.models.User,
						attributes: [],
						include: [
							{
								model: ctx.models.Company,
								attributes: [],
								include: [
									{
										model: ctx.models.IndustryTranslation,
										attributes: [],
									},
								],
							},
						],
					},
					{
						model: ctx.models.RegistrationStatus,
						attributes: [],
					},
				],
				attributes: [
					[col('user.id') as any, 'id'],
					[col('user.company.industry.name') as any, 'industryName'],
					[col('user.company.name') as any, 'companyName'],
					[col('user.email') as any, 'email'],
					[col('user.createdAt') as any, 'createdAt'],
				],
			});
		} catch (err) {
			console.log('err', err);
		}
	}
}
