import { InputType, Field } from 'type-graphql';
import { IsEmail } from 'class-validator';

@InputType()
export class LoginUserInput {
	@Field()
	@IsEmail()
	email: string;

	@Field()
	password: string;
}

@InputType()
export class RestorePasswordInput {
	@Field()
	@IsEmail()
	email: string;
}

@InputType()
export class RestorePasswordConfirmInput {
	@Field()
	token: string;

	@Field()
	password: string;
}
