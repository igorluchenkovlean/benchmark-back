import { Resolver, Ctx, Arg, Mutation } from 'type-graphql';
import { compare } from 'bcrypt';
import { ApolloError, AuthenticationError } from 'apollo-server';

import { UnauthContext } from 'gql/context';
import { models } from 'db';
import { redis } from '../../../redis';
import { AuthResult } from 'gql/types';
import { LoginUserInput } from './inputs';
import {
	limiterConsecutiveFailsByUsernameAndIP,
	limiterSlowBruteByIP,
	maxWrongAttemptsByIPperDay,
	maxConsecutiveFailsByUsernameAndIP,
} from './utils/rateLimiterUtils';
import { getJWTRefreshToken, getJWTToken } from 'utils/jwt';

@Resolver()
export class AuthResolver {
	@Mutation(() => AuthResult)
	async loginUser(@Ctx() ctx: UnauthContext, @Arg('params') { email, password }: LoginUserInput) {
		try {
			const user = await ctx.models.User.findOne({
				where: { email },
				include: [
					{
						model: models.Role,
					},
					{
						model: models.Language,
					},
					{
						model: models.Company,
					},
					{
						model: models.Company,
					},
				],
			});

			const ipAddr = ctx.req.ip;
			const usernameIPkey = `${email}_${ipAddr}`;

			const [resUsernameAndIP, resSlowByIP] = await Promise.all([
				limiterConsecutiveFailsByUsernameAndIP.get(usernameIPkey),
				limiterSlowBruteByIP.get(ipAddr),
			]);

			let retrySecs = 0;

			if (resSlowByIP !== null && resSlowByIP.consumedPoints > maxWrongAttemptsByIPperDay) {
				retrySecs = Math.round(resSlowByIP.msBeforeNext / 1000) || 1;
			} else if (resUsernameAndIP !== null && resUsernameAndIP.consumedPoints > maxConsecutiveFailsByUsernameAndIP) {
				retrySecs = Math.round(resUsernameAndIP.msBeforeNext / 1000) || 1;
			}

			if (retrySecs > 0) {
				ctx.res.set('Retry-After', String(retrySecs));
				return new ApolloError('Too Many Requests', '429');
			}

			const matched = user && (await compare(password, user.password));

			if (!user || !matched) {
				try {
					const limiterPromises = [limiterSlowBruteByIP.consume(ipAddr)];
					if (user) {
						limiterPromises.push(limiterConsecutiveFailsByUsernameAndIP.consume(usernameIPkey));
					}

					await Promise.all(limiterPromises);

					return new AuthenticationError('Invalid credentials');
				} catch (rlRejected) {
					if (rlRejected instanceof Error) {
						throw rlRejected;
					} else {
						ctx.res.set('Retry-After', String(Math.round(rlRejected.msBeforeNext / 1000)) || 1);
						return new ApolloError('Too Many Requests', '429');
					}
				}
			}

			const tokenLive = 1000 * 60 * 60 * 24; // 1d
			const refreshTokenLive = 1000 * 60 * 60 * 24 * 14; // 14d

			if (user) {
				if (resUsernameAndIP !== null && resUsernameAndIP.consumedPoints > 0) {
					await limiterConsecutiveFailsByUsernameAndIP.delete(usernameIPkey);
				}

				const refreshToken = getJWTRefreshToken(user);
				const token = getJWTToken(user);

				ctx.res.cookie('refreshToken', refreshToken, { maxAge: 900000, httpOnly: true });

				redis.set(token, JSON.stringify(user), 'EX', tokenLive);
				redis.set(refreshToken, JSON.stringify(user), 'EX', refreshTokenLive);

				const isHasAnyCompanyDetails = await ctx.models.Company.findOne({
					where: {
						id: user.company.id,
					},
				});

				return {
					token,
					isInitialData: isHasAnyCompanyDetails && isHasAnyCompanyDetails.city ? true : false,
				};
			}
		} catch (err) {
			console.log('err', err);
		}
	}
}
