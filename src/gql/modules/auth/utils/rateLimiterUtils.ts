import { RateLimiterRedis } from 'rate-limiter-flexible';

import { redis } from '../../../../redis';

export const maxWrongAttemptsByIPperDay = 100;
export const maxConsecutiveFailsByUsernameAndIP = 10;

export const limiterSlowBruteByIP = new RateLimiterRedis({
	storeClient: redis,
	keyPrefix: 'login_fail_ip_per_day',
	points: maxWrongAttemptsByIPperDay,
	duration: 60 * 60 * 24,
	blockDuration: 60 * 60 * 24, // Block for 1 day, if 100 wrong attempts per day
});

export const limiterConsecutiveFailsByUsernameAndIP = new RateLimiterRedis({
	storeClient: redis,
	keyPrefix: 'login_fail_consecutive_username_and_ip',
	points: maxConsecutiveFailsByUsernameAndIP,
	duration: 60 * 60 * 24 * 90, // Store number for 90 days since first fail
	blockDuration: 60 * 15, // Block for 15 min
});
