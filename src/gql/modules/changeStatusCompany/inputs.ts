import { InputType, Field } from 'type-graphql';

@InputType()
export class ReqChangeStatusCompany {
	@Field()
	userId: number;

	@Field()
	status: string;
}
