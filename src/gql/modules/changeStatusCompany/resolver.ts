import { Resolver, Mutation, Ctx, Arg, UseMiddleware } from 'type-graphql';
import { ApolloError } from 'apollo-server';

import { Context } from 'gql/context';
import { isAuthorized } from 'gql/middleware';
import UserRegistrationStatus from 'db/models/userRegistrationStatus.model';
import { ReqChangeStatusCompany } from './inputs';
import { col } from 'sequelize';
import { RegisteredUsersCompanies } from 'gql/types';

@Resolver()
export class ChangeStatusCompanyResolver {
	@UseMiddleware(isAuthorized)
	@Mutation(() => RegisteredUsersCompanies)
	async changeStatusCompany(@Ctx() ctx: Context, @Arg('params') { userId, status }: ReqChangeStatusCompany) {
		try {
			if (ctx.user.roleId !== 1) {
				return new ApolloError('Permission denied', '403');
			}

			const userRegistrationStatus = (await ctx.models.UserRegistrationStatus.findOne({
				where: {
					userId,
				},
			})) as UserRegistrationStatus;

			await userRegistrationStatus.update(
				{
					updatedAt: new Date(),
				},
				{
					where: {
						userId,
					},
				},
			);

			await ctx.models.RegistrationStatus.update(
				{
					name: status,
				},
				{
					where: {
						id: userRegistrationStatus.registrationStatusId,
					},
				},
			);

			const userRegistrationStatusNew = await ctx.models.UserRegistrationStatus.findOne({
				raw: true,
				where: {
					userId,
				},
				include: [
					{
						model: ctx.models.User,
						attributes: [],
						include: [
							{
								model: ctx.models.Company,
								attributes: [],
								include: [
									{
										model: ctx.models.IndustryTranslation,
										attributes: [],
									},
								],
							},
						],
					},
					{
						model: ctx.models.RegistrationStatus,
						attributes: [],
					},
					{
						model: ctx.models.ReservedDomain,
						attributes: [],
					},
				],
				attributes: [
					[col('user.id') as any, 'id'],
					[col('user.company.industry.name') as any, 'industryName'],
					[col('user.company.name') as any, 'companyName'],
					[col('user.company.postalCode') as any, 'postalCode'],
					[col('reservedDomain.name') as any, 'siteName'],
					[col('user.email') as any, 'email'],
					[col('user.company.employeesNumber') as any, 'employeesNumber'],
				],
			});

			return userRegistrationStatusNew;
		} catch (err) {
			console.log('err', err);
		}
	}
}
