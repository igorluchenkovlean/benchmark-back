import { Resolver, Query, UseMiddleware } from 'type-graphql';

import { isAuthorized } from 'gql/middleware';
import { Countries } from 'gql/types';
import { countriesData } from 'utils/countriesData';

@Resolver()
export class CountriesResolver {
	@UseMiddleware(isAuthorized)
	@Query(() => [Countries])
	getCountries() {
		return countriesData;
	}
}
