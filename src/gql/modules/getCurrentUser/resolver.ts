import { Resolver, Query, Ctx, UseMiddleware } from 'type-graphql';

import { Context } from 'gql/context';
import { isAuthorized } from 'gql/middleware';
import { UserDetails } from 'gql/types';

@Resolver()
export class getCurrentUserResolver {
	@UseMiddleware(isAuthorized)
	@Query(() => UserDetails)
	async getCurrentUser(@Ctx() ctx: Context) {
		try {
			const user = await ctx.models.User.findOne({
				where: { id: ctx.user.id },
				include: [
					{
						model: ctx.models.Role,
					},
					{
						model: ctx.models.Language,
					},
					{
						model: ctx.models.Company,
						include: [
							{
								model: ctx.models.IndustryTranslation,
							},
						],
					},
				],
			});

			const userRegistrationStatus = await ctx.models.UserRegistrationStatus.findOne({
				where: {
					userId: ctx.user.id,
				},
				include: [
					{
						model: ctx.models.RegistrationStatus,
					},
					{
						model: ctx.models.ReservedDomain,
					},
				],
			});

			return {
				user,
				userRegistrationStatus,
			};
		} catch (err) {
			console.log('err', err);
		}
	}
}
