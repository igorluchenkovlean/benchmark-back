import { InputType, Field } from 'type-graphql';

@InputType()
export class ReqRegisteredUsersCompanies {
	@Field({ nullable: true })
	offset: number;

	@Field({ nullable: true })
	limit: number;

	@Field({ nullable: true })
	status: string;
}
