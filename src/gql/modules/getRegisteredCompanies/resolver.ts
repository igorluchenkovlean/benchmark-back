import { Resolver, Ctx, Arg, UseMiddleware, Query } from 'type-graphql';
import { col } from 'sequelize';

import { Context } from 'gql/context';
import { isAuthorized } from 'gql/middleware';
import { ReqRegisteredUsersCompanies } from './inputs';
import { RegisteredUsersCompanies } from 'gql/types';

@Resolver()
export class GetRegisteredCompaniesResolver {
	@UseMiddleware(isAuthorized)
	@Query(() => [RegisteredUsersCompanies])
	async getRegisteredCompanies(@Ctx() ctx: Context, @Arg('params') params: ReqRegisteredUsersCompanies) {
		try {
			return ctx.models.UserRegistrationStatus.findAll({
				raw: true,
				where: {
					['$registrationStatus.name$']: params.status,
					['$user.roleId$']: 2,
				},
				subQuery: false,
				include: [
					{
						model: ctx.models.User,
						attributes: [],
						include: [
							{
								model: ctx.models.Company,
								attributes: [],
								include: [
									{
										model: ctx.models.IndustryTranslation,
										attributes: [],
									},
								],
							},
						],
					},
					{
						model: ctx.models.RegistrationStatus,
						attributes: [],
					},
					{
						model: ctx.models.ReservedDomain,
						attributes: [],
					},
				],
				attributes: [
					'id',
					[col('user.id') as any, 'userId'],
					[col('user.company.industry.name') as any, 'industryName'],
					[col('user.company.name') as any, 'companyName'],
					[col('user.company.postalCode') as any, 'postalCode'],
					[col('reservedDomain.name') as any, 'siteName'],
					[col('user.email') as any, 'email'],
					[col('user.company.employeesNumber') as any, 'employeesNumber'],
				],
			});
		} catch (err) {
			console.log('err', err);
		}
	}
}
