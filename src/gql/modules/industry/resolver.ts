import { Resolver, Query, UseMiddleware, Ctx } from 'type-graphql';

import { isAuthorized } from 'gql/middleware';
import { Industries } from 'gql/types';
import { Context } from 'gql/context';

@Resolver()
export class IndustriesResolver {
	@UseMiddleware(isAuthorized)
	@Query(() => [Industries])
	async getIndustries(@Ctx() ctx: Context) {
		try {
			return ctx.models.GroupTranslation.findAll({
				attributes: ['id', 'name'],
			});
		} catch (err) {
			console.log('err', err);
		}
	}
}
