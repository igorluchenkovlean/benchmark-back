import { Resolver, Query, Ctx, UseMiddleware } from 'type-graphql';

import { Context } from 'gql/context';
import { isAuthorized } from 'gql/middleware';
import { User } from 'gql/types';

@Resolver()
export class MeResolver {
	@UseMiddleware(isAuthorized)
	@Query(() => User)
	me(@Ctx() ctx: Context) {
		return ctx.user;
	}
}
