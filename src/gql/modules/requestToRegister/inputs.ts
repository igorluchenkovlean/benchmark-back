import { IsEmail } from 'class-validator';

import { InputType, Field } from 'type-graphql';

@InputType()
export class RandomUserParams {
	@Field()
	firstName: string;

	@Field()
	lastName: string;

	@IsEmail()
	@Field()
	userEmail: string;

	@Field()
	companyName: string;
}
