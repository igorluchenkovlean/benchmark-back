import { Resolver, Arg, Mutation } from 'type-graphql';

import { RandomUserParams } from './inputs';
import { sendEmail } from 'services/mailer';

@Resolver()
export class RequestToRegisterResolver {
	@Mutation(() => Boolean)
	async requestToRegister(@Arg('params') params: RandomUserParams) {
		try {
			await sendEmail({
				type: 'requestToRegister',
				payload: {
					...params,
					email: process.env.ADMIN_EMAIL as string,
				},
			});

			return true;
		} catch (err) {
			console.log('err', err);
		}
	}
}
