import { InputType, Field } from 'type-graphql';

@InputType()
export class ReqServices {
	@Field()
	id: number;

	@Field()
	companyGroupServiceId: number;

	@Field({ nullable: true })
	value: number;

	@Field()
	label: string;
}
