import { Resolver, Query, Ctx, UseMiddleware, Arg, Mutation } from 'type-graphql';
import { col } from 'sequelize';

import { Context } from 'gql/context';
import { isAuthorized } from 'gql/middleware';
import { ServiceGroup, CompanyGroupService, Service, CompaniesToApprovalData, UpdateServiceValue } from 'gql/types';
import CompanyGroup from 'db/models/companyGroup.model';
import ICompanyGroupService from 'db/models/companyGroupService.model';
import { ReqServices } from './inputs';
import { models } from 'db';

@Resolver()
export class ServiceGrouprResolver {
	@UseMiddleware(isAuthorized)
	@Query(() => [ServiceGroup])
	async getCompanyGroups(@Ctx() ctx: Context) {
		try {
			const сompanyGroups = await ctx.models.CompanyGroup.findAll({
				where: {
					userId: ctx.user.id,
				},
				include: [{ all: true }],
			});

			return сompanyGroups.map(сompanyGroup => {
				return {
					companyGroupId: сompanyGroup.group.id,
					serviceGroupName: сompanyGroup.group.name,
					servicesCount: сompanyGroup.services.length,
				};
			});
		} catch (err) {
			console.log('err', err);
		}
	}

	@UseMiddleware(isAuthorized)
	@Query(() => [CompanyGroupService])
	async getCompanyGroupServices(@Ctx() ctx: Context, @Arg('groupId') groupId: number) {
		try {
			const companyGroup = (await ctx.models.CompanyGroup.findOne({
				where: {
					userId: ctx.user.id,
					groupId,
				},
			})) as CompanyGroup;

			const companyGroupServices = (await ctx.models.CompanyGroupService.findAll({
				raw: true,
				where: {
					companyGroupId: companyGroup.id,
				},
				include: [{ all: true, attributes: [] }],
				attributes: ['id', ['id', 'companyGroupServiceId'], [col('service.name') as any, 'serviceName']],
			})) as ICompanyGroupService[];

			return Promise.all(
				companyGroupServices.map(async item => {
					const isSubmit = await ctx.models.PendingDataRequest.findOne({
						where: {
							companyGroupServiceId: item.id,
							status: 'approval',
						},
					});
					return {
						...item,
						isSubmit: isSubmit ? true : false,
					};
				}),
			);
		} catch (err) {
			console.log('err', err);
		}
	}

	@UseMiddleware(isAuthorized)
	@Query(() => [Service])
	async getService(@Ctx() ctx: Context, @Arg('companyGroupServiceId') companyGroupServiceId: number) {
		try {
			return ctx.models.CompanyGroupServiceRawField.findAll({
				raw: true,
				where: {
					companyGroupServiceId,
				},
				include: [
					{
						model: ctx.models.GroupServiceRawField,
						include: [
							{
								model: ctx.models.RawFieldTranslation,
							},
						],
					},
				],

				attributes: [
					'id',
					'companyGroupServiceId',
					'value',
					[col('groupServiceRawField.rawField.name') as any, 'label'],
				],
			});
		} catch (err) {
			console.log('err', err);
		}
	}

	@UseMiddleware(isAuthorized)
	@Mutation(() => UpdateServiceValue)
	async updateServiceValue(@Ctx() ctx: Context, @Arg('params', () => [ReqServices]) params: ReqServices[]) {
		try {
			for (const param of params) {
				await ctx.models.CompanyGroupServiceRawField.update(
					{
						value: param.value,
						updatedAt: new Date(),
					},
					{
						where: {
							id: param.id,
						},
					},
				);
			}

			await ctx.models.PendingDataRequest.update(
				{
					status: 'approval',
				},
				{
					where: {
						id: params[0].companyGroupServiceId,
					},
				},
			);

			return {
				companyGroupServiceId: params[0].companyGroupServiceId,
				isSubmit: true,
			};
		} catch (err) {
			console.log('err', err);
		}
	}

	@UseMiddleware(isAuthorized)
	@Query(() => [CompaniesToApprovalData])
	async getCompaniesToApproval(@Ctx() ctx: Context, @Arg('status') status: string) {
		try {
			const pendingDataRequests = await ctx.models.PendingDataRequest.findAll({
				raw: true,
				where: {
					status,
				},
				include: [
					{
						model: models.CompanyGroupService,
						include: [
							{
								model: models.CompanyGroup,
								include: [{ model: models.GroupTranslation }, { model: models.User }],
							},
							{ model: models.ServiceTranslation },
						],
					},
					{ model: models.Company, include: [{ model: models.IndustryTranslation }] },
				],

				attributes: [
					'companyGroupServiceId',
					['id', 'pendingDataRequestId'],
					[col('companyGroupService.companyGroup.group.name') as any, 'groupName'],
					[col('company.industry.name') as any, 'industryName'],
					[col('companyGroupService.service.name') as any, 'serviceName'],
					[col('company.name') as any, 'companyName'],
					[col('companyGroupService.companyGroup.user.email') as any, 'email'],
					[col('companyGroupService.companyGroup.user.id') as any, 'userId'],
				],
			});

			return pendingDataRequests;
		} catch (err) {
			console.log('err', err);
		}
	}
}
