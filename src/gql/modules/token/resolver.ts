import { Resolver, Mutation, Arg } from 'type-graphql';

import { AuthenticationError } from 'apollo-server';
import { redis } from '../../../redis';
import { Token } from 'gql/types/Token';
import { getJWTToken } from 'utils/jwt';

@Resolver()
export class TokenResolver {
	@Mutation(() => Token)
	async getNewAccessToken(@Arg('params') refreshToken: string) {
		try {
			const hasRefreshToken = await redis.get(refreshToken);
			if (hasRefreshToken) {
				const parse = JSON.parse(hasRefreshToken);
				const token = getJWTToken(parse);

				const tokenLive = 1000 * 60 * 60 * 24;
				redis.set(token, JSON.stringify(parse), 'EX', tokenLive);

				return { token };
			} else {
				throw new AuthenticationError('Token is incorrect');
			}
		} catch (err) {
			console.log('err', err);
		}
	}
}
