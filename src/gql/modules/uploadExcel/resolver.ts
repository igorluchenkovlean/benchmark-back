import { Resolver, Mutation, Arg, UseMiddleware, Ctx } from 'type-graphql';
import * as ExcelJS from 'exceljs';

import { isAuthorized } from 'gql/middleware';
import { Context } from 'gql/context';
import { getRows } from './utils/getRows';
import { addGroupService } from './utils/addGroupService';
import { removeOldData } from './utils/removeOldData';
import { ServiceGroup } from 'gql/types';

@Resolver()
export class UploadExcelResolver {
	@UseMiddleware(isAuthorized)
	@Mutation(() => [ServiceGroup])
	async uploadFile(@Ctx() ctx: Context, @Arg('file') file: string) {
		try {
			const app = await removeOldData(ctx.user);
			if (app) {
				const buf = Buffer.from(file, 'base64');
				const workbook = new ExcelJS.Workbook();

				const rows = await getRows(workbook, buf);

				for (const row of rows) {
					for (const key in row) {
						if (key && typeof row[key] === 'string') {
							await addGroupService(row, +key, ctx.user);
						}
					}
				}

				const сompanyGroups = await ctx.models.CompanyGroup.findAll({
					where: {
						userId: ctx.user.id,
					},
					include: [{ all: true }],
				});

				return сompanyGroups.map(сompanyGroup => {
					return {
						companyGroupId: сompanyGroup.group.id,
						serviceGroupName: сompanyGroup.group.name,
						servicesCount: сompanyGroup.services.length,
					};
				});
			}
		} catch (err) {
			console.log('err', err);
		}
	}
}
