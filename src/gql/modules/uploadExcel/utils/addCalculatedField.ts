import models from 'db/models';

export const addCalculatedField = async (
	formula: string,
	name: string,
	languageCode: number,
	serviceGroupId: number,
) => {
	try {
		const derivativeField = await models.DerivativeField.create({
			formula,
		});

		await models.DerivativeFieldTranslation.create({
			name,
			languageCode,
			derivativeFieldId: derivativeField.id,
		});

		await models.GroupServiceDerivativeField.create({
			serviceGroupId,
			derivativeFieldId: derivativeField.id,
		});

		return derivativeField.id;
	} catch (err) {
		console.log('err', err);
	}
};
