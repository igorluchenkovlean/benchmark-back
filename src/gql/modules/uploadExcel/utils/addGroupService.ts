import User from 'db/models/user.model';
import models from 'db/models';
import { addCalculatedField } from './addCalculatedField';
import { IParams } from './types';

export const addGroupService = async (row: IParams, key: number, user: User) => {
	try {
		const groupTranslation = await models.GroupTranslation.findOne({
			where: {
				name: row[key].replace(new RegExp('\\r?\\n', 'g'), ''),
			},
		});

		if (groupTranslation) {
			const companyGroup = await models.CompanyGroup.findOrCreate({
				defaults: { groupId: groupTranslation.groupId, userId: user.id, updatedAt: new Date() },
				where: { groupId: groupTranslation.groupId, userId: user.id },
			});

			for (const index in row) {
				if (typeof row[index] === 'string') {
					const serviceTranslation = await models.ServiceTranslation.findOne({
						raw: true,
						where: {
							name: row[index].replace(new RegExp('\\r?\\n', 'g'), ''),
						},
					});

					if (serviceTranslation) {
						const companyGroupService = await models.CompanyGroupService.create({
							serviceId: serviceTranslation.serviceId,
							companyGroupId: companyGroup[0].id,
							updatedAt: new Date(),
						});

						await models.PendingDataRequest.create({
							companyId: user.companyId,
							status: 'pending',
							companyGroupServiceId: companyGroupService.id,
							updatedAt: new Date(),
						});

						const numerator = await models.Numerator.create();

						await models.NumeratorTranslation.create({
							name: row[2].toString(),
							numeratorId: numerator.id,
							languageCode: user.languageCode,
						});

						const groupService = await models.GroupService.create({
							groupId: groupTranslation.groupId,
							serviceId: serviceTranslation.serviceId,
							numeratorId: numerator.id,
						});

						const rawField = await models.RawField.create();

						const headersRowFieldEN = [
							'Total cost of service',
							'Cost of projects (thereof)',
							'Degree of Outsourcing in %',
							'Value Numerator',
							'Remarks',
						];

						for (const headerName of headersRowFieldEN) {
							const rawFieldTranslation = await models.RawFieldTranslation.create({
								languageCode: user.languageCode,
								rawFieldId: rawField.id,
								name: headerName,
							});

							const groupServiceRawField = await models.GroupServiceRawField.create({
								serviceGroupId: groupService.id,
								rawFieldId: rawFieldTranslation.id,
							});

							await models.CompanyGroupServiceRawField.create({
								groupServiceRawFieldId: groupServiceRawField.id,
								companyGroupServiceId: companyGroupService.id,
								updatedAt: new Date(),
							});
						}

						const groupServiceRawFields = await models.GroupServiceRawField.findAll({
							raw: true,
							where: {
								serviceGroupId: groupService.id,
							},
						});

						const totalCostWithoutCoPId = await addCalculatedField(
							`groupServiceRawFields${groupServiceRawFields[0].id} - groupServiceRawFields${groupServiceRawFields[1].id}`,
							'Total Cost without CoP (F - G)',
							user.languageCode,
							groupService.id,
						);

						if (totalCostWithoutCoPId) {
							await addCalculatedField(
								`totalCostWithoutCoPId${totalCostWithoutCoPId} * groupServiceRawFields${groupServiceRawFields[3].id}`,
								'Products for calculating the outsourcing level of the total costs',
								user.languageCode,
								groupService.id,
							);

							await addCalculatedField(
								`groupServiceRawFields${groupServiceRawFields[0].id}`,
								'Total cost for which outsourcing degree is indicated',
								user.languageCode,
								groupService.id,
							);

							const kpiField = await models.KPIField.create({
								formula: `totalCostWithoutCoPId${totalCostWithoutCoPId} * 1000 / groupServiceRawFields${groupServiceRawFields[4].id}`,
							});

							await models.KPIFieldTranslation.create({
								name: 'KPI',
								languageCode: user.languageCode,
								kpiFieldId: kpiField.id,
							});

							await models.GroupServiceKPIField.create({
								serviceGroupId: groupService.id,
								kpiFieldId: kpiField.id,
							});
						}
					}
				}
			}
		}
	} catch (err) {
		console.log('err', err);
	}
};
