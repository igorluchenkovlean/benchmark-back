import { Workbook } from 'exceljs';

import { IParams } from './types';

export const getRows = (workbook: Workbook, buf: Buffer): Promise<IParams[]> => {
	const rows = [] as IParams[];
	return new Promise(resolve => {
		workbook.xlsx.load(buf).then(workbook => {
			workbook.eachSheet(sheet => {
				sheet.eachRow((row, rowIndex) => {
					rows.push({ ...row.values, rowIndex });
				});
			});
			resolve(rows);
		});
	});
};
