import CompanyGroupServiceRawField from 'db/models/companyGroupServiceRawField.model';
import models from 'db/models';

export const removeCompanyGroupServiceRawField = async (
	companyGroupServiceRawFields: CompanyGroupServiceRawField[],
) => {
	try {
		for (const companyGroupServiceRawField of companyGroupServiceRawFields) {
			await models.CompanyGroupServiceRawField.destroy({
				where: {
					id: companyGroupServiceRawField.id,
				},
			});
		}
	} catch (err) {
		console.log('err', err);
	}
};
