import models from 'db/models';

export const removeDerivativeFields = async (derivativeFieldsIds: number[]) => {
	try {
		for (const derivativeFieldId of derivativeFieldsIds) {
			await models.DerivativeFieldTranslation.destroy({
				where: {
					derivativeFieldId,
				},
			});

			await models.DerivativeField.destroy({
				where: {
					id: derivativeFieldId,
				},
			});
		}
	} catch (err) {
		console.log('err', err);
	}
};
