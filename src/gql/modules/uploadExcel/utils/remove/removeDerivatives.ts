import models from 'db/models';

export const removeDerivatives = async (
	groupServiceRawFieldsIDs: {
		rawFieldId: number;
		serviceGroupId: number;
	}[],
) => {
	try {
		const derivativeFieldsIds = [];
		for (const groupServiceRawField of groupServiceRawFieldsIDs) {
			const derivativeFieldTranslations = await models.GroupServiceDerivativeField.findOne({
				where: {
					serviceGroupId: groupServiceRawField.serviceGroupId,
				},
			});

			if (derivativeFieldTranslations) {
				derivativeFieldsIds.push(derivativeFieldTranslations.derivativeFieldId);
			}

			await models.GroupServiceDerivativeField.destroy({
				where: {
					serviceGroupId: groupServiceRawField.serviceGroupId,
				},
			});
		}

		return derivativeFieldsIds;
	} catch (err) {
		console.log('err', err);
	}
};
