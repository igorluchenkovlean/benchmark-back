import models from 'db/models';

export const removeGroupServiceKPIField = async (
	groupServiceRawFieldsIDs: {
		rawFieldId: number;
		serviceGroupId: number;
	}[],
) => {
	try {
		const groupServiceKPIFieldsIds = [];
		for (const groupServiceRawField of groupServiceRawFieldsIDs) {
			const groupServiceKPIField = await models.GroupServiceKPIField.findOne({
				where: {
					serviceGroupId: groupServiceRawField.serviceGroupId,
				},
			});

			if (groupServiceKPIField) {
				groupServiceKPIFieldsIds.push(groupServiceKPIField.kpiFieldId);
			}

			await models.GroupServiceKPIField.destroy({
				where: {
					serviceGroupId: groupServiceRawField.serviceGroupId,
				},
			});
		}

		return groupServiceKPIFieldsIds;
	} catch (err) {
		console.log('err', err);
	}
};
