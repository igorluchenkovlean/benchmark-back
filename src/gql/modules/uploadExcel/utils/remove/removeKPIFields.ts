import models from 'db/models';

export const removeKPIFields = async (groupServiceKPIFieldIds: number[]) => {
	try {
		for (const kpiFieldId of groupServiceKPIFieldIds) {
			await models.KPIFieldTranslation.destroy({
				where: {
					kpiFieldId,
				},
			});

			await models.KPIField.destroy({
				where: {
					id: kpiFieldId,
				},
			});
		}
	} catch (err) {
		console.log('err', err);
	}
};
