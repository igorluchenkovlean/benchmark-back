import models from 'db/models';

export const removeNumerators = async (numeratorsIds: number[]) => {
	try {
		for (const numeratorId of numeratorsIds) {
			await models.NumeratorTranslation.destroy({
				where: {
					numeratorId,
				},
			});

			await models.Numerator.destroy({
				where: {
					id: numeratorId,
				},
			});
		}
	} catch (err) {
		console.log('err', err);
	}
};
