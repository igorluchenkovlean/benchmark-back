import models from 'db/models';

export const removeRawFieldAndGroupService = async (
	groupServiceRawFieldsIDs: {
		rawFieldId: number;
		serviceGroupId: number;
	}[],
) => {
	try {
		const numeratorsIds = [];
		for (const groupServiceRawField of groupServiceRawFieldsIDs) {
			const groupService = await models.GroupService.findOne({
				where: {
					id: groupServiceRawField.serviceGroupId,
				},
			});

			if (groupService) {
				numeratorsIds.push(groupService.numeratorId);
			}

			await models.RawField.destroy({
				where: {
					id: groupServiceRawField.rawFieldId,
				},
			});

			await models.GroupService.destroy({
				where: {
					id: groupServiceRawField.serviceGroupId,
				},
			});
		}

		return numeratorsIds;
	} catch (err) {
		console.log('err', err);
	}
};
