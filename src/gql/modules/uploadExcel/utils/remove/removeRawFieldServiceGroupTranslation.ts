import models from 'db/models';
import GroupServiceRawField from 'db/models/groupServiceRawField.model';
import CompanyGroupServiceRawField from 'db/models/companyGroupServiceRawField.model';
import RawFieldTranslation from 'db/models/rawFieldTranslation.model';

export const removeRawFieldServiceGroupTranslation = async (
	companyGroupServiceRawFields: CompanyGroupServiceRawField[],
) => {
	try {
		const groupServiceRawFieldsIDs = [];

		for (const companyGroupServiceRawField of companyGroupServiceRawFields) {
			const groupServiceRawField = (await models.GroupServiceRawField.findOne({
				where: {
					id: companyGroupServiceRawField.groupServiceRawFieldId,
				},
			})) as GroupServiceRawField;

			const rawFieldTranslation = (await models.RawFieldTranslation.findOne({
				where: {
					id: companyGroupServiceRawField.groupServiceRawFieldId,
				},
			})) as RawFieldTranslation;

			groupServiceRawFieldsIDs.push({
				rawFieldId: rawFieldTranslation.rawFieldId,
				serviceGroupId: groupServiceRawField.serviceGroupId,
			});

			await models.GroupServiceRawField.destroy({
				where: {
					id: companyGroupServiceRawField.groupServiceRawFieldId,
				},
			});

			await models.RawFieldTranslation.destroy({
				where: {
					id: groupServiceRawField.rawFieldId,
				},
			});
		}

		return groupServiceRawFieldsIDs;
	} catch (err) {
		console.log('err', err);
	}
};
