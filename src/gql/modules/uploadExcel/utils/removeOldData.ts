import User from 'db/models/user.model';
import models from 'db/models';
import CompanyGroupService from 'db/models/companyGroupService.model';
import CompanyGroupServiceRawField from 'db/models/companyGroupServiceRawField.model';
import {
	removeNumerators,
	removeRawFieldAndGroupService,
	removeRawFieldServiceGroupTranslation,
	removeCompanyGroupServiceRawField,
	removeDerivatives,
	removeDerivativeFields,
	removeGroupServiceKPIField,
	removeKPIFields,
} from './remove';

export const removeOldData = async (user: User) => {
	try {
		const hasCompanyGroup = await models.CompanyGroup.findAll({
			where: {
				userId: user.id,
			},
		});

		if (hasCompanyGroup && hasCompanyGroup.length) {
			for (const item of hasCompanyGroup) {
				const companyGroupId = item.id;

				const companyGroupServices = (await models.CompanyGroupService.findAll({
					raw: true,
					where: {
						companyGroupId,
					},
				})) as CompanyGroupService[];

				for (const companyGroupService of companyGroupServices) {
					const companyGroupServiceRawFields = (await models.CompanyGroupServiceRawField.findAll({
						raw: true,
						where: {
							companyGroupServiceId: companyGroupService.id,
						},
					})) as CompanyGroupServiceRawField[];

					await removeCompanyGroupServiceRawField(companyGroupServiceRawFields);

					const groupServiceRawFieldsIDs = await removeRawFieldServiceGroupTranslation(companyGroupServiceRawFields);

					if (groupServiceRawFieldsIDs && groupServiceRawFieldsIDs.length) {
						const derivativeFieldsIds = await removeDerivatives(groupServiceRawFieldsIDs);

						if (derivativeFieldsIds && derivativeFieldsIds.length) {
							await removeDerivativeFields(derivativeFieldsIds);
						}

						const groupServiceKPIFieldIds = await removeGroupServiceKPIField(groupServiceRawFieldsIDs);

						if (groupServiceKPIFieldIds && groupServiceKPIFieldIds.length) {
							await removeKPIFields(groupServiceKPIFieldIds);
						}

						const numeratorsIds = await removeRawFieldAndGroupService(groupServiceRawFieldsIDs);

						if (numeratorsIds && numeratorsIds.length) {
							await removeNumerators(numeratorsIds);
						}
					}

					await models.PendingDataRequest.destroy({
						where: {
							companyGroupServiceId: companyGroupService.id,
						},
					});

					await models.CompanyGroupService.destroy({
						where: {
							id: companyGroupService.id,
						},
					});
				}
			}

			await models.CompanyGroup.destroy({
				where: {
					userId: user.id,
				},
			});
		}

		return true;
	} catch (err) {
		console.log('err', err);
	}
};
