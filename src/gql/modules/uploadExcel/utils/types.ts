export interface IParams {
	[x: number]: string;
	rowIndex: number;
}
