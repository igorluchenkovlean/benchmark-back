import { IsEmail } from 'class-validator';

import { InputType, Field } from 'type-graphql';
import { UserLanguage } from 'typedefs/user';

@InputType()
export class RegisterUserParams {
	@Field()
	firstName: string;

	@Field()
	lastName: string;

	@Field()
	password: string;

	@Field(() => UserLanguage)
	language: UserLanguage;

	@Field()
	token: string;
}

@InputType()
export class UpdatePassword {
	@Field()
	@IsEmail()
	token: string;

	@Field()
	password: string;
}

@InputType()
export class addCompany {
	@Field()
	@IsEmail()
	email: string;

	@Field()
	industryName: string;

	@Field()
	companyName: string;

	@Field(() => UserLanguage)
	language: UserLanguage;
}

@InputType()
export class addCompanyDetails {
	@Field()
	employeesNumber: number;

	@Field()
	country: string;

	@Field()
	city: string;

	@Field()
	postalCode: string;

	@Field()
	siteName: string;
}

@InputType()
export class ReqUserAndCompanyData {
	@Field({ nullable: true })
	firstName: string;

	@Field({ nullable: true })
	lastName: string;

	@Field({ nullable: true })
	employeesNumber: number;

	@Field({ nullable: true })
	country: string;

	@Field({ nullable: true })
	city: string;

	@Field({ nullable: true })
	postalCode: string;

	@Field({ nullable: true })
	siteName: string;
}
