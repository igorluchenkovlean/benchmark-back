import { Resolver, Query, Ctx, Arg, Mutation, UseMiddleware } from 'type-graphql';
import { ApolloError } from 'apollo-server';
import { hash, compare } from 'bcrypt';
import { sha3_256 } from 'js-sha3';

import { Context } from 'gql/context';
import { User, AuthResult, UserDetails } from 'gql/types';
import { UserRole, UserRegisterStatus } from 'typedefs/user';
import Language from 'db/models/language.model';
import UserRegistrationStatus from 'db/models/userRegistrationStatus.model';
import { sendEmail } from 'services/mailer';
import { redis } from 'redis';
import { getJWTRefreshToken, getJWTToken } from 'utils/jwt';
import { isAuthorized } from 'gql/middleware';
import { RegisterUserParams, UpdatePassword, ReqUserAndCompanyData, addCompany, addCompanyDetails } from './inputs';

@Resolver()
export class UserResolver {
	@Mutation(() => AuthResult)
	async registerUser(@Ctx() ctx: Context, @Arg('params') params: RegisterUserParams) {
		try {
			if (!Object.keys(params).includes('token') && !params.token) {
				return new ApolloError("Token doesn't exists", '400');
			}

			const hasToken = await redis.get(params.token);

			if (!hasToken) {
				return new ApolloError("Token doesn't exists", '400');
			}

			const token = JSON.parse(hasToken);

			const hasUser = await ctx.models.User.findOne({
				where: {
					email: token.email,
				},
			});

			const userRegistrationStatus = await ctx.models.UserRegistrationStatus.findOne({
				where: {
					userId: hasUser && hasUser.id,
				},
			});

			await ctx.models.RegistrationStatus.update(
				{
					name: UserRegisterStatus.completed,
				},
				{
					where: {
						id: userRegistrationStatus && userRegistrationStatus.registrationStatusId,
					},
				},
			);

			await ctx.models.User.update(
				{
					firstName: params.firstName,
					lastName: params.lastName,
					password: await hash(params.password, 10),
				},
				{
					where: {
						email: token.email,
					},
				},
			);

			const tokenLive = 1000 * 60 * 60 * 24; // 1d
			const refreshTokenLive = 1000 * 60 * 60 * 24 * 14; // 14d

			const user = await ctx.models.User.findOne({
				where: { email: token.email },
				include: [
					{
						model: ctx.models.Role,
					},
					{
						model: ctx.models.Language,
					},
					{
						model: ctx.models.Company,
					},
				],
			});

			if (!user) {
				return new ApolloError("User doesn't exists", '400');
			}

			const refreshToken = getJWTRefreshToken(user);
			const accessToken = getJWTToken(user);

			ctx.res.cookie('refreshToken', refreshToken, { maxAge: 900000, httpOnly: true });

			redis.set(token, JSON.stringify(user), 'EX', tokenLive);
			redis.set(refreshToken, JSON.stringify(user), 'EX', refreshTokenLive);

			await sendEmail({
				type: 'registration',
				payload: {
					firstName: params.firstName,
					lastName: params.lastName,
					email: token.email,
				},
			});

			await redis.del(params.token);

			return {
				token: accessToken,
				isInitialData: false,
			};
		} catch (err) {
			console.log('err', err);
		}
	}

	@Mutation(() => Boolean)
	async addCompany(@Ctx() ctx: Context, @Arg('params') params: addCompany) {
		try {
			const hasUser = await ctx.models.User.findOne({
				where: {
					email: params.email,
				},
			});

			if (hasUser) {
				return new ApolloError('User already exists', '401');
			}

			const hasCompany = await ctx.models.Company.findOne({
				where: {
					name: params.companyName,
				},
			});

			if (hasCompany) {
				return new ApolloError('Company already exists', '401');
			}

			const langauge = (await ctx.models.Language.findOne({
				where: {
					name: params.language,
				},
			})) as Language;

			const Industry = await ctx.models.Industry.create();

			await ctx.models.IndustryTranslation.create({
				name: params.industryName,
				industryId: Industry.id,
				languageCode: langauge.code,
			});

			const company = await ctx.models.Company.create({
				name: params.companyName,
				industryId: Industry.id,
			});

			const user = await ctx.models.User.create({
				email: params.email,
				companyId: company.id,
				industryId: Industry.id,
				roleId: 2,
				languageCode: langauge.code,
			});

			await ctx.models.UserRole.create({
				userId: user.id,
				roleId: 2,
			});

			const token = sha3_256(
				Math.random()
					.toString(36)
					.substr(2, 36),
			);

			const registrationStatus = await ctx.models.RegistrationStatus.create({
				name: UserRegisterStatus.pending,
			});

			await ctx.models.UserRegistrationStatus.create({
				userId: user.id,
				registrationStatusId: registrationStatus.id,
				token,
			});

			redis.set(token, JSON.stringify({ email: params.email, company }), 'EX', 60 * 60 * 24 * 14);

			const host = ctx.req.headers.origin as string;

			await sendEmail({
				type: 'sendInvite',
				payload: {
					host,
					token,
					email: params.email,
				},
			});

			return true;
		} catch (err) {
			console.log('err', err);
		}
	}

	@Mutation(() => Boolean)
	async forgotPassword(@Ctx() ctx: Context, @Arg('email') email: string) {
		try {
			const hasUser = await ctx.models.User.findOne({
				where: {
					email,
				},
			});

			if (!hasUser) {
				return new ApolloError("User doesn't exists", '401');
			}

			const userRegistrationStatus = await ctx.models.UserRegistrationStatus.findOne({
				where: {
					userId: hasUser.id,
				},
				include: [
					{
						model: ctx.models.RegistrationStatus,
					},
				],
			});

			const isActiveUser =
				userRegistrationStatus && userRegistrationStatus.registrationStatus.name === UserRegisterStatus.pending;

			if (isActiveUser) {
				return new ApolloError("User doesn't active", '401');
			}

			const token = sha3_256(
				Math.random()
					.toString(36)
					.substr(2, 36),
			);

			redis.set(token, JSON.stringify({ email }), 'EX', 60 * 60 * 24);

			const host = ctx.req.headers.origin as string;

			await sendEmail({
				type: 'forgotPassword',
				payload: {
					host,
					token,
					email,
				},
			});

			return true;
		} catch (err) {
			console.log('err', err);
		}
	}

	@UseMiddleware(isAuthorized)
	@Mutation(() => Boolean)
	async addCompanyDetails(@Ctx() ctx: Context, @Arg('params') params: addCompanyDetails) {
		try {
			const hasSiteName = await ctx.models.ReservedDomain.findOne({
				where: {
					name: params.siteName,
				},
			});

			if (hasSiteName) {
				return new ApolloError('Domain name is already exists', '400');
			}

			const reservedDomain = await ctx.models.ReservedDomain.create({
				name: params.siteName,
			});

			const userRegistrationStatus = await ctx.models.UserRegistrationStatus.findOne({
				where: {
					userId: ctx.user.id,
				},
			});

			userRegistrationStatus &&
				(await userRegistrationStatus.update({
					reservedDomainId: reservedDomain.id,
					updatedAt: new Date(),
				}));

			await ctx.models.Company.update(
				{
					country: params.country,
					employeesNumber: params.employeesNumber,
					city: params.city,
					postalCode: params.postalCode,
				},
				{
					where: {
						id: ctx.user.companyId,
					},
				},
			);

			return true;
		} catch (err) {
			console.log('err', err);
		}
	}

	@Query(() => [User])
	async users(@Ctx() ctx: Context, @Arg('roles', () => [UserRole], { nullable: true }) roles?: [UserRole]) {
		return ctx.models.User.findAll({ where: roles ? { role: roles } : {}, order: [['id', 'asc']] });
	}

	@Query(() => User)
	async user(@Ctx() ctx: Context, @Arg('id') id: number) {
		const user = await ctx.models.User.findByPk(id);
		if (!user) throw new ApolloError('User not found', '404');

		return user;
	}

	@Mutation(() => Boolean)
	async updatePassword(@Ctx() ctx: Context, @Arg('params') params: UpdatePassword) {
		try {
			if (!Object.keys(params).includes('token') && !params.token) {
				return new ApolloError("Token doesn't exists", '400');
			}

			const hasToken = await redis.get(params.token);

			if (!hasToken) {
				return new ApolloError("Token doesn't exists", '400');
			}

			const token = JSON.parse(hasToken);
			const user = await ctx.models.User.findOne({
				where: {
					email: token.email,
				},
			});
			if (!user) throw new ApolloError('User not found', '404');
			await user.update({
				password: await hash(params.password, 10),
			});

			await redis.del(params.token);

			return true;
		} catch (err) {
			console.log('err', err);
		}
	}

	@UseMiddleware(isAuthorized)
	@Mutation(() => Boolean)
	async changeUserAndCompanyData(@Ctx() ctx: Context, @Arg('params') params: ReqUserAndCompanyData) {
		try {
			const reservedDomain = await ctx.models.ReservedDomain.findOne({
				where: {
					name: params.siteName,
				},
			});

			const userRegistrationStatus = (await ctx.models.UserRegistrationStatus.findOne({
				where: {
					userId: ctx.user.id,
				},
				include: [
					{
						model: ctx.models.ReservedDomain,
					},
				],
			})) as UserRegistrationStatus;

			const hasDomainName = reservedDomain && reservedDomain.name !== userRegistrationStatus.reservedDomain.name;

			if (hasDomainName) {
				return new ApolloError('Domain name is already exists', '400');
			}

			await ctx.models.ReservedDomain.update(
				{
					name: params.siteName,
				},
				{
					where: {
						id: userRegistrationStatus.reservedDomainId,
					},
				},
			);

			await ctx.models.User.update(
				{
					firstName: params.firstName,
					lastName: params.lastName,
				},
				{
					where: {
						id: ctx.user.id,
					},
				},
			);

			await ctx.models.Company.update(
				{
					country: params.country,
					employeesNumber: params.employeesNumber,
					city: params.city,
					postalCode: params.postalCode,
				},
				{
					where: {
						id: ctx.user.companyId,
					},
				},
			);

			return true;
		} catch (err) {
			console.log('err', err);
		}
	}

	@UseMiddleware(isAuthorized)
	@Mutation(() => Boolean)
	async changePassword(@Ctx() ctx: Context, @Arg('password') password: string) {
		try {
			const user = await ctx.models.User.findOne({
				where: {
					id: ctx.user.id,
				},
			});

			const matched = await compare(password, ctx.user.password);

			if (matched) {
				return new ApolloError('The password cannot be changed to the old', '400');
			}

			if (user) {
				await user.update({
					password: await hash(password, 10),
				});
			}

			return true;
		} catch (err) {
			console.log('err', err);
		}
	}

	@UseMiddleware(isAuthorized)
	@Mutation(() => Boolean)
	async logOut(@Ctx() ctx: Context) {
		try {
			redis.del(ctx.req.headers.authorization as string);

			return true;
		} catch (err) {
			console.log('err', err);
		}
	}

	@UseMiddleware(isAuthorized)
	@Query(() => UserDetails)
	async getUserData(@Ctx() ctx: Context, @Arg('userId') userId: number) {
		try {
			const user = await ctx.models.User.findOne({
				where: { id: userId },
				include: [
					{
						model: ctx.models.Role,
					},
					{
						model: ctx.models.Language,
					},
					{
						model: ctx.models.Company,
						include: [
							{
								model: ctx.models.IndustryTranslation,
							},
						],
					},
				],
			});

			const userRegistrationStatus = await ctx.models.UserRegistrationStatus.findOne({
				where: {
					userId: userId,
				},
				include: [
					{
						model: ctx.models.RegistrationStatus,
					},
					{
						model: ctx.models.ReservedDomain,
					},
				],
			});

			return {
				user,
				userRegistrationStatus,
			};
		} catch (err) {
			console.log('err', err);
		}
	}
}
