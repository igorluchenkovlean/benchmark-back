import { Resolver, Ctx, Arg, Mutation, UseMiddleware } from 'type-graphql';
import { compare } from 'bcrypt';

import { Context } from 'gql/context';
import { isAuthorized } from 'gql/middleware';
import User from 'db/models/user.model';

@Resolver()
export class ValidateOldPasswordResolver {
	@UseMiddleware(isAuthorized)
	@Mutation(() => Boolean)
	async isOldPassword(@Ctx() ctx: Context, @Arg('password') password: string) {
		try {
			const user = (await ctx.models.User.findByPk(ctx.user.id)) as User;

			return compare(password, user.password);
		} catch (err) {
			console.log('err', err);
		}
	}
}
