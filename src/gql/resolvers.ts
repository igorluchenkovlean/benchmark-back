import { readdirSync } from 'fs';
import { resolve } from 'path';

const fileNamesToImport = ['resolver.ts', 'fields.ts', 'resolver.js', 'fields.js'];

export const resolvers: Array<Function | string> = [];

const modulesDir = resolve(__dirname, 'modules');
readdirSync(modulesDir).forEach((name: string) => {
	const moduleDir = resolve(modulesDir, name);
	readdirSync(moduleDir)
		.filter((fileName: string) => fileNamesToImport.includes(fileName))
		.forEach((fileName: string) => resolvers.push(require(resolve(moduleDir, fileName)).default));
});
