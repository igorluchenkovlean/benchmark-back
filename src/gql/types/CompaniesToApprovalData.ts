import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class CompaniesToApprovalData {
	@Field()
	pendingDataRequestId: number;

	@Field()
	companyGroupServiceId: number;

	@Field()
	companyName: string;

	@Field()
	industryName: string;

	@Field()
	groupName: string;
	@Field()
	serviceName: string;

	@Field()
	email: string;

	@Field()
	userId: number;
}
