import { ObjectType, Field } from 'type-graphql';
import { IndustryTranslation } from './IndustryTranslation';

@ObjectType()
export class Company {
	@Field()
	id: number;

	@Field({ nullable: true })
	siteName: string;

	@Field()
	name: string;

	@Field({ nullable: true })
	employeesNumber: number;

	@Field({ nullable: true })
	industry: IndustryTranslation;

	@Field({ nullable: true })
	country: string;

	@Field({ nullable: true })
	city: string;

	@Field({ nullable: true })
	postalCode: string;
}
