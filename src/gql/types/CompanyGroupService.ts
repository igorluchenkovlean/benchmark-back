import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class CompanyGroupService {
	@Field()
	companyGroupServiceId: number;

	@Field()
	serviceName: string;

	@Field()
	isSubmit: boolean;
}
