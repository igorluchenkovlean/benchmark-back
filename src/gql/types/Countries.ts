import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class Countries {
	@Field()
	name: string;

	@Field()
	code: string;
}
