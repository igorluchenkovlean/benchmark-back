import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class Industries {
	@Field()
	id: string;

	@Field()
	name: string;
}
