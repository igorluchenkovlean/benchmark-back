import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class IndustryTranslation {
	@Field()
	industryId: number;

	@Field()
	name: string;
}
