import { ObjectType, Field } from 'type-graphql';
import { UserLanguage } from 'typedefs/user';

@ObjectType()
export class Language {
	@Field()
	code: number;

	@Field()
	name: UserLanguage;
}
