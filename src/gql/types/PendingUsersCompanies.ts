import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class PendingUsersCompanies {
	@Field()
	id: number;

	@Field()
	industryName: string;

	@Field()
	companyName: string;

	@Field()
	email: string;

	@Field()
	createdAt: Date;
}
