import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class RegisteredUsersCompanies {
	@Field()
	id: number;

	@Field()
	industryName: string;

	@Field()
	companyName: string;

	@Field()
	postalCode: string;

	@Field()
	siteName: string;

	@Field()
	email: string;

	@Field()
	employeesNumber: number;

	@Field()
	userId: number;
}
