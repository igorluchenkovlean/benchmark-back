import { ObjectType, Field } from 'type-graphql';
import { UserRegisterStatus } from 'typedefs/user';

@ObjectType()
export class RegistrationStatus {
	@Field()
	id: number;

	@Field()
	name: UserRegisterStatus;
}
