import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class ReservedDomain {
	@Field()
	id: number;

	@Field()
	name: string;

	@Field()
	createdAt: Date;
}
