import { ObjectType, Field } from 'type-graphql';
import { UserRole } from 'typedefs/user';

@ObjectType()
export class Role {
	@Field()
	id: number;

	@Field()
	label: UserRole;
}
