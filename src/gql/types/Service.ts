import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class Service {
	@Field()
	id: number;

	@Field()
	companyGroupServiceId: number;

	@Field({ nullable: true })
	value: number;

	@Field()
	label: string;
}
