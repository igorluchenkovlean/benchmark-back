import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class ServiceGroup {
	@Field()
	companyGroupId: number;

	@Field()
	serviceGroupName: string;

	@Field()
	servicesCount: number;
}
