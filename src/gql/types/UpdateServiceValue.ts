import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class UpdateServiceValue {
	@Field()
	companyGroupServiceId: number;

	@Field()
	isSubmit: string;
}
