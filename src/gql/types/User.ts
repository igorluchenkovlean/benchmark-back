import { ObjectType, Field } from 'type-graphql';

import { IsEmail } from 'class-validator';
import { Company } from './Company';
import { Role } from './Role';
import { Language } from './Language';

@ObjectType()
export class User {
	@Field()
	id: number;

	@Field()
	fullName: string;

	@Field()
	firstName: string;

	@Field()
	lastName: string;

	@Field()
	language: Language;

	@Field()
	company: Company;

	@Field()
	role: Role;

	@Field()
	@IsEmail()
	email: string;
}
