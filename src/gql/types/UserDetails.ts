import { ObjectType, Field } from 'type-graphql';
import { User } from 'gql/types/User';
import { UserRegistrationStatus } from 'gql/types/UserRegistrationStatus';

@ObjectType()
export class UserDetails {
	@Field()
	user: User;

	@Field()
	userRegistrationStatus: UserRegistrationStatus;
}
