import { ObjectType, Field } from 'type-graphql';

import { User } from './User';
import { ReservedDomain } from './ReservedDomain';
import { RegistrationStatus } from './RegistrationStatus';

@ObjectType()
export class UserRegistrationStatus {
	@Field()
	user: User;

	@Field()
	registrationStatus: RegistrationStatus;

	@Field()
	reservedDomain: ReservedDomain;

	@Field()
	updatedAt: Date;

	@Field()
	createdAt: Date;

	@Field()
	token: string;
}
