import { GraphQLResolveInfo } from 'graphql';
import { parseResolveInfo } from 'graphql-parse-resolve-info';
import { Model } from 'sequelize-typescript';

import { models } from 'db';
import { parseTree } from './parseTree';

interface Include {
	model: typeof Model;
	as: string;
	include?: Include[];
}

export const getInclude = (info: GraphQLResolveInfo) => {
	const tree = parseTree(parseResolveInfo(info, { deep: true, keepRoot: false }) as { [key: string]: any });

	if (!tree) return [];

	const includes: Array<Include> = [];
	Object.keys(tree).forEach(key => {
		const innerTree = { ...tree[key] };
		delete innerTree.model;

		const innerTreeExists = Object.values(innerTree).length > 0;

		const include: Include = {
			model: models[tree[key].model as keyof typeof models],
			as: key,
		};
		if (innerTreeExists) {
			include.include = getInclude(innerTree);
		}

		includes.push(include);
	});

	return includes;
};
