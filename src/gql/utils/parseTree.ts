import { models } from 'db';

export const parseTree = (tree: { [key: string]: any }) => parseSubTree(Object.values(tree.fieldsByTypeName)[0]);

function parseSubTree(tree: any) {
	const fields: { [key: string]: any } = {};

	Object.keys(tree).forEach(key => {
		const { fieldsByTypeName } = tree[key];
		if (!fieldsByTypeName) return;

		const [modelName] = Object.keys(fieldsByTypeName);
		if (modelName && models[modelName as keyof typeof models]) {
			fields[key] = {
				...parseSubTree(fieldsByTypeName[modelName]),
				model: modelName,
			};
		}
	});

	return fields;
}
