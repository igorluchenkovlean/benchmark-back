import 'reflect-metadata';
import { ApolloServer } from 'apollo-server-express';
import * as toobusy from 'toobusy-js';
import connectRedis = require('connect-redis');
import cors = require('cors');
import Express = require('express');
import session = require('express-session');
import cookieParser = require('cookie-parser');
import bodyParser = require('body-parser');
import morgan = require('morgan');

import { createSchema, context } from 'gql';
import { redis, rateLimiterRedisMiddleware } from './redis';
import { logger } from './logger';

const runServer = async () => {
	const apolloServer = new ApolloServer({
		schema: await createSchema(),
		context,
		introspection: true,
		playground: true,
	});

	const app = Express();

	const RedisStore = connectRedis(session);

	app.use(morgan('combined', { stream: logger.stream }));

	app.use(rateLimiterRedisMiddleware);

	app.use(
		cors({
			credentials: true,
			origin: ['http://localhost:3000', 'http://localhost:3001'],
		}),
	);

	app.use(
		session({
			store: new RedisStore({
				client: redis as any,
			}),
			name: process.env.SESSION_NAME,
			secret: process.env.SECRET as string,
			resave: false,
			saveUninitialized: false,
			cookie: {
				httpOnly: true,
				secure: process.env.NODE_ENV === 'production',
				maxAge: 1000 * 60 * 60 * 24, // 1 day
			},
		}),
	);

	app.use(cookieParser());

	app.use(
		bodyParser.json({
			limit: '10mb',
		}),
	);

	app.use(
		bodyParser.urlencoded({
			limit: '10mb',
			parameterLimit: 100000,
			extended: true,
		}),
	);

	app.use((_, res, next) => {
		if (toobusy()) {
			res.status(429).send('Too Many Requests');
		} else {
			next();
		}
	});

	apolloServer.applyMiddleware({ app, cors: false });

	const port = process.env.PORT;

	app.listen(process.env.PORT, () => {
		console.log(`🚀 Server started on http://localhost:${port}/graphql`);
	});
};

runServer().catch(console.error);
