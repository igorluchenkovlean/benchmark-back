import * as appRoot from 'app-root-path';
import { createLogger, config, transports, format } from 'winston';

const options = {
	file: {
		level: 'info',
		filename: `${appRoot}/logs/info.log`,
		handleExceptions: true,
		maxsize: 5242880, // 5MB
		maxFiles: 5,
		colorize: false,
	},
	error: {
		level: 'error',
		filename: `${appRoot}/logs/error.log`,
		handleExceptions: true,
		json: false,
		maxsize: 5242880, // 5MB
		maxFiles: 5,
		colorize: false,
	},
	console: {
		level: 'debug',
		handleExceptions: true,
		json: false,
	},
};

export const logger = new (createLogger as any)({
	levels: config.syslog.levels,
	format: format.combine(format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }), format.prettyPrint()),
	transports: [
		new transports.File(options.file),
		new transports.Console(options.console),
		new transports.File(options.error),
	],
	exitOnError: false,
});

logger.stream = {
	write: (message: string) => {
		logger.info(message);
	},
};
