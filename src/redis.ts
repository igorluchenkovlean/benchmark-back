import { Request, Response } from 'express';
import { RateLimiterRedis } from 'rate-limiter-flexible';
import Redis from 'ioredis';

export const redis = new Redis({
	host: process.env.REDIS_HOST,
	port: process.env.REDIS_PORT as any,
	password: process.env.REDIS_PASSWORD,
	enableOfflineQueue: false,
});

const opts = {
	storeClient: redis,
	points: 10,
	duration: 1,
};

const rateLimiterRedis = new RateLimiterRedis(opts);

export const rateLimiterRedisMiddleware = (req: Request, res: Response, next: () => void) => {
	rateLimiterRedis
		.consume(req.connection.remoteAddress as string)
		.then(() => {
			next();
		})
		.catch(() => {
			res.status(429).send('Too Many Requests');
		});
};
