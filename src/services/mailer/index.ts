import { createTransport } from 'nodemailer';

import { templates, SendEmailPayload } from './templates';

export const sendEmail = async ({ type, payload }: SendEmailPayload) => {
	try {
		const compiledTemplate = templates[type](payload as any);
		const smtpTransport = createTransport({
			service: process.env.EMAIL_SERVICE,
			debug: true,
			logger: true,
			auth: {
				user: process.env.EMAIL_USER,
				pass: process.env.EMAIL_PASS,
			},
		});

		const mailOptions = {
			to: payload.email,
			from: process.env.EMAIL_USER,
			subject: compiledTemplate.title,
			html: compiledTemplate.body,
		};

		return smtpTransport.sendMail(mailOptions);
	} catch (err) {
		console.log('err', err);
	}
};
