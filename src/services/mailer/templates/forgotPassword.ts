export default ({ host, token }: { host: string; token: string }) => ({
	title: 'Reset Password',
	body: `Hello,
	<a href="${host}/reset-password/${token}">Click Reset Password</a>
	`,
});
