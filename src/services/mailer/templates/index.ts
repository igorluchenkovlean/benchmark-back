import registration from './registration';
import sendInvite from './sendInvite';
import forgotPassword from './forgotPassword';
import requestToRegister from './requestToRegister';

export const templates = {
	registration,
	sendInvite,
	forgotPassword,
	requestToRegister,
};

export type SendEmailPayload =
	| {
			type: 'registration';
			payload: Parameters<typeof registration>[0] & { email: string };
	  }
	| {
			type: 'sendInvite';
			payload: Parameters<typeof sendInvite>[0] & { email: string };
	  }
	| {
			type: 'forgotPassword';
			payload: Parameters<typeof forgotPassword>[0] & { email: string };
	  }
	| {
			type: 'requestToRegister';
			payload: Parameters<typeof requestToRegister>[0] & { email: string };
	  };
