export default ({ firstName, lastName, email }: { firstName: string; lastName: string; email: string }) => ({
	title: 'You were successfully registered',
	body: `Hello, ${firstName} ${lastName}
	<p>Your login: <strong>${email}</strong></p>
	`,
});
