export default ({
	firstName,
	lastName,
	companyName,
	userEmail,
}: {
	firstName: string;
	lastName: string;
	companyName: string;
	userEmail: string;
}) => ({
	title: 'Request to Register',
	body: `
    Hello, My name is ${firstName} ${lastName}.
	My company name is ${companyName}.
	Email for contact: ${userEmail}.
    I would like to register on your website. What should I do for it?
	`,
});
