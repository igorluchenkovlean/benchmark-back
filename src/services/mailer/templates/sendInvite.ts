export default ({ host, token }: { host: string; token: string }) => ({
	title: 'Registration',
	body: `Hello,
	<a href="${host}/register/${token}">sign</a>
	`,
});
