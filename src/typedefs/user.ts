import User from 'db/models/user.model';

export enum UserRole {
	superAdmin = 'super admin',
	producer = 'producer',
}

export enum UserLanguage {
	en = 'en',
	ge = 'ge',
}

export enum UserRegisterStatus {
	pending = 'pending',
	completed = 'completed',
	removed = 'removed',
}

export enum ExcelDataStatus {
	pending = 'pending',
	approval = 'approval',
	approved = 'approved',
	error = 'error',
}

export type UserInJWT = Readonly<
	Pick<User, 'id' | 'firstName' | 'lastName' | 'email' | 'role' | 'language' | 'company'>
>;
