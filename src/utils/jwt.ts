import { sign } from 'jsonwebtoken';

import { UserInJWT } from 'typedefs/user';

export const getJWTToken = (user: UserInJWT) => sign(JSON.stringify(user), process.env.JWT_SECRET_KEY as string);

export const getJWTRefreshToken = (user: UserInJWT) =>
	sign(JSON.stringify(user), process.env.JWT_REFRESH_TOKEN_SECRET_KEY as string);
